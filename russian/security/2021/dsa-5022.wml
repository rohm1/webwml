#use wml::debian::translation-check translation="9c44c7905cd7bbe6df67ff227e8eaec739c984bb" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>

<p>Было обнаружено, что исправление уязвимости <a
href="https://security-tracker.debian.org/tracker/CVE-2021-44228">\
CVE-2021-44228</a> в Apache Log4j, платформе ведения журнала для Java,
неполно для некоторых настроек, отличающихся от настроек по умолчанию. Это может позволить
злоумышленникам, управляющим входными данными карты контекстов потоков (MDC), если
в настройках ведения журнала используется разметка шаблона не по умолчанию либо с
поиском контекста (например, $${ctx:loginId}), либо шаблон карты контекстов потоков (%X, %mdc или %MDC),
формировать вредоносные входные данные, используя шаблон поиска JNDI, приводящие к
отказу в обслуживании (DOS).</p>

<p>В предыдущем стабильном выпуске (buster) эта проблема была исправлена
в версии 2.16.0-1~deb10u1.</p>

<p>В стабильном выпуске (bullseye) эта проблема была исправлена в
версии 2.16.0-1~deb11u1.</p>

<p>Рекомендуется обновить пакеты apache-log4j2.</p>

<p>С подробным статусом поддержки безопасности apache-log4j2 можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5022.data"
