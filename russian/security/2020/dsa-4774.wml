#use wml::debian::translation-check translation="43f8d7b8b91b167696b5c84ec0911bab7b7073f2" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В ядре Linux было обнаружено несколько уязвимостей, которые могут
приводить к выполнению произвольного кода, повышению привилегий,
отказу в обслуживании или утечкам информации.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12351">CVE-2020-12351</a>

    <p>Энди Нгуен обнаружил уязвимость в реализации Bluetooth в способе обработки
    L2CAP-пакетов с A2MP CID. Удалённый злоумышленник, находящийся на небольшом
    расстоянии и знающий адрес Bluetooth-устройства жертвы, может отправить
    вредоносный l2cap-пакет, что вызовет отказ в обслуживании или выполнение произвольного
    кода с правами ядра.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12352">CVE-2020-12352</a>

    <p>Энди Нгуен обнаружил уязвимость в реализации Bluetooth. Память стека
    инициализируется неправильно при обработке определённых AMP-пакетов.
    Удалённый злоумышленник, находящийся на небольшом расстоянии и знающий адрес
    Bluetooth-устройства жертвы, может получить информацию о стеке ядра.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25211">CVE-2020-25211</a>

    <p>В подсистеме netfilter была обнаружена уязвимость. Локальный злоумышленник,
    способный ввести настройки conntrack Netlink, может вызвать отказ
    в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25643">CVE-2020-25643</a>

    <p>Чен Нан из Chaitin Security Research Lab обнаружил уязвимость в
    модуле hdlc_ppp. Неправильная очистка входных данных в функции ppp_cp_parse_cr()
    может приводить к повреждению содержимого памяти и раскрытию информации.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25645">CVE-2020-25645</a>

    <p>В драйвере интерфейса для инкапсулированного трафика GENEVE при объединении
    с IPsec, была обнаружена уязвимость. Если IPsec настроен на шифрование трафика
    для определённого UDP-порта, используемого GENEVE-туннелем, то
    туннелированые данные маршрутизируются неправильно по зашифрованному
    каналу и отправляются в незашифрованном виде.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 4.19.152-1. Уязвимости исправлены путём перехода на новую стабильную
версию из основной ветки разработки, 4.19.152, которая включает
дополнительные исправления ошибок.</p>

<p>Рекомендуется обновить пакеты linux.</p>

<p>С подробным статусом поддержки безопасности linux можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4774.data"
