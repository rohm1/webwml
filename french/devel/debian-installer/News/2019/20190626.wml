#use wml::debian::translation-check translation="a4e1569c494a4c453a82c868fa62cec78e894157" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Buster RC2</define-tag>
<define-tag release_date>2019-06-26</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la seconde version candidate pour Debian 10 <q>Buster</q>.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>choose-mirror :
    <ul>
      <li>mise à jour de Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>cryptsetup :
    <ul>
      <li>nouvelle section « déverrouillage des périphériques LUKS à partir
        de GRUB » pointant vers :
        <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html</a>.</li>
    </ul>
  </li>
  <li>debian-archive-keyring :
    <ul>
      <li>ajout des clés de Buster (<a href="https://bugs.debian.org/917535">nº 917535</a>, <a href="https://bugs.debian.org/917536">nº 917536</a>).</li>
    </ul>
  </li>
  <li>debian-cd :
    <ul>
      <li>Création d'images pour s'adapter également aux clés USB 16 Go
        (pour amd64 et i386) ;</li>
      <li>adaptation de la sélection de paquets pour que l'installation
        par le réseau multi-architecture avec les microprogrammes tienne
        à nouveau sur un CD (nécessite un CD-R de 700 Mo) : les noyaux
        686 PAE ne sont pas inclus dans ces CD ;</li>
      <li>adaptation de l'ordre des URL des instantanés de l'archive dans
        les images jigdo pour supprimer la charge sur snapshot.debian.org.</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>ajout de haveged-udeb [linux] pour éviter les problèmes de
        manque d'entropie (<a href="https://bugs.debian.org/923675">nº 923675</a>)
        qui pourraient affecter les connexions HTTPS, la génération
        de paires de clés SSH, etc ;</li>
      <li>passage de l'ABI du noyau Linux de la version 4.19.0-4 à la
        version 4.19.0-5 ;</li>
      <li>ajout des glyphes œ/Œ pour la traduction française ;</li>
      <li>mise à jour des limites de taille ;</li>
      <li>renommage du thème « Dark » en « Accessible à contraste élevé »
        (<a href="https://bugs.debian.org/930569">nº 930569</a>) ;</li>
      <li>compression des images u-boot armhf avec « gzip -n » pour éviter
        d'incorporer des horodatages qui peuvent poser des problèmes de
        reproductibilité.</li>
    </ul>
  </li>
  <li>espeakup :
    <ul>
      <li>attente plus longue des cartes son.</li>
    </ul>
  </li>
  <li>grub2 :
    <ul>
      <li>recommandation d'efibootmgr par grub-efi-*-bin aux fins de
        débogage ;</li>
      <li>fonctionnement de grub-efi aussi sur armhf (corrections par
        l'amont de bogues d'alignement).</li>
    </ul>
  </li>
  <li>installation-guide :
    <ul>
      <li>ajout du réglage de partman-auto-lvm/guided_size pour le fichier
        de configuration d'exemple de préconfiguration
        (<a href="https://bugs.debian.org/930846">nº 930846</a>).</li>
    </ul>
  </li>
  <li>libdebian-installer :
    <ul>
      <li>augmentation de la longueur maximale des lignes dans les paquets
        et les fichiers source
        (<a href="https://bugs.debian.org/554444">nº 554444</a>).</li>
    </ul>
  </li>
  <li>lowmem :
    <ul>
      <li>mise à jour des limites de taille.</li>
    </ul>
  </li>
  <li>network-console :
    <ul>
      <li>correction d'une erreur de segmentation de gen-crypt qui empêchait
        des installations à distance due à l'absence de mot de passe pour
        pour l'utilisateur « installer » (<a href="https://bugs.debian.org/926947">nº 926947</a>,
        <a href="https://bugs.debian.org/928299">nº 928299</a>).</li>
    </ul>
  </li>
  <li>openssl :
    <ul>
      <li>fourniture d'un fichier openssl.cnf dans libssl1.1-udeb,
        corrigeant des problèmes de TLS de wget dans l'installateur
        (<a href="https://bugs.debian.org/926315">nº 926315</a>).</li>
    </ul>
  </li>
  <li>partman-auto :
    <ul>
      <li>adaptation de la traduction en arabe pour éviter un blocage à
        l'étape du disque dur (<a href="https://bugs.debian.org/929877">nº 929877</a>).</li>
    </ul>
  </li>
  <li>preseed :
    <ul>
      <li>mise à jour de auto-install/defaultroot, en remplaçant Stretch
        par Buster (<a href="https://bugs.debian.org/928031">nº 928031</a>).</li>
    </ul>
  </li>
  <li>rootskel :
    <ul>
      <li>lancement de haveged au moment opportun pour éviter le manque
        d'entropie (<a href="https://bugs.debian.org/923675">nº 923675</a>).
        C'est-à-dire lorsque l'exécutable de haveged est disponible et
        qu'il n'y a pas de générateur de nombres aléatoires matériel ;</li>
      <li>mise à jour des limites de taille pour l'installateur graphique.</li>
    </ul>
  </li>
</ul>


<h2>Mise à jour du démarrage sécurisé d'UEFI (« Secure Boot »)</h2>

<p>La configuration de Secure Boot dans Debian est encore peaufinée, les
principales mises à jour sont résumées ci-dessous.</p>

<ul>
  <li>debian-installer :
    <ul>
      <li>ajout de shim-signed et grub-efi-ARCH-signed aux dépendances de
        constructions pour amd64/i386/arm64 ;</li>
      <li>utilisation des paquets signés de shim et grub pour les trois
        architectures pour les images EFI ;</li>
      <li>correction de la configuration de l'amorçage depuis le réseau
        pour les images signées de grub pour correspondre à la configuration
        précédente et à la documentation existante
        (<a href="https://bugs.debian.org/928750">nº 928750</a>).</li>
    </ul>
  </li>
  <li>grub2 :
    <ul>
      <li>création d'une image d'amorçage par le réseau particulière pour
        que l'installateur Debian s'en serve
        (<a href="https://bugs.debian.org/928750">nº 928750</a>) ;</li>
      <li>ajout des modules cpuid, play et ntfs aux images signées d'UEFI
        (<a href="https://bugs.debian.org/928628">nº 928628</a>, <a href="https://bugs.debian.org/930290">nº 930290</a>, <a href="https://bugs.debian.org/923855">nº 923855</a>) ;</li>
      <li>traitement de --force-extra-removable également avec shim signé
      (<a href="https://bugs.debian.org/930531">nº 930531</a>).</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
    <ul>
      <li>[arm64] ajout de la prise en charge des images de carte SD pour
        l'amorçage par le réseau ;</li>
      <li>[arm64] ajout des images u-boot pour a64-olinuxino,
        orangepi_zero_plus2 et teres_i ;</li>
      <li>ajout de la prise en charge de NanoPi NEO2.</li>
    </ul>
  </li>
  <li>flash-kernel :
    <ul>
      <li>ajout de la prise en charge de NanoPi NEO2 (<a href="https://bugs.debian.org/928861">nº 928861</a>) ;</li>
      <li>ajout de la prise en charge de Marvell 8040 MACCHIATOBin
        Double-shot et Single-shot (<a href="https://bugs.debian.org/928951">nº 928951</a>).</li>
    </ul>
  </li>
  <li>linux :
    <ul>
      <li>udeb : ajout de tous les pilotes HWRNG à kernel-image (<a href="https://bugs.debian.org/923675">nº 923675</a>).</li>
      <li>udeb : input-modules : inclusion de tous les modules de pilote de clavier ;</li>
      <li>[arm64] udeb : kernel-image : inclusion des pilotes cros_ec_spi et SPI ;</li>
      <li>[arm64] udeb : kernel-image : inclusion de phy-rockchip-pcie ;</li>
      <li>[arm64] udeb : usb-modules : inclusion de phy-rockchip-typec et
        extcon-usbc-cros-ec ;</li>
      <li>[arm64] udeb : mmc-modules : inclusion de phy-rockchip-emmc ;</li>
      <li>[arm64] udeb : fb-modules : inclusion de rockchipdrm, panel-simple,
        pwm_bl et pwm-cros-ec ;</li>
      <li>udeb : suppression des paquets ntfs-modules inutilisés.</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>76 langues sont prises en charge dans cette version ;</li>
  <li>La traduction est complète pour 39 de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
