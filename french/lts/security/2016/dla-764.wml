#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans QEMU :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9911">CVE-2016-9911</a>

<p>Quick Emulator (Qemu) construit avec la prise en charge de l'émulation de
l'interface EHCI USB est vulnérable à un problème de fuite de mémoire. Il
pourrait survenir lors du traitement de données de paquet dans
<q>ehci_init_transfer</q>. Un utilisateur ou un processus client pourrait
utiliser ce problème pour divulguer la mémoire de l'hôte, avec pour
conséquence un déni de service pour l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9921">CVE-2016-9921</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9922">CVE-2016-9922</a>

<p>Quick emulator (Qemu) construit avec la prise en charge de l'émulateur
VGA Cirrus CLGD 54xx est vulnérable à un problème de division par zéro. Il
pourrait survenir lors de la copie de données VGA quand le mode graphique de
Cirrus est réglé à VGA. Un utilisateur privilégié dans un client pourrait
utiliser ce défaut pour planter l'instance du processus Qemu sur l'hôte,
avec pour conséquence un déni de service.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.1.2+dfsg-6+deb7u19.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-764.data"
# $Id: $
