#use wml::debian::translation-check translation="3ef6628a8e034f9dc948b059aa43b54b22e5fa9d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans zabbix,
une solution de surveillance réseau. Un utilisateur authentifié peut créer
un lien avec du code Javascript réfléchi à l'intérieur pour les pages de
graphiques, d'actions et de services et l'envoie à d'autres utilisateurs.
La charge utile peut seulement être exécutée avec une valeur connue du
jeton CSRF de la victime qui est changée périodiquement et est difficile à
prédire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1:3.0.32+dfsg-0+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets zabbix.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de zabbix, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/zabbix">\
https://security-tracker.debian.org/tracker/zabbix</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2980.data"
# $Id: $
