#use wml::debian::translation-check translation="93f7a4e38946db3bfc79818ca8f56d1012518098" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans libgit2, une
bibliothèque Git de bas niveau :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8098">CVE-2018-8098</a>

<p>Un dépassement d'entier dans la fonction index.c:read_entry() pendant la
décompression de la longueur du préfixe compressée dans les versions de
libgit2 antérieures à v0.26.2 permet à un attaquant de provoquer un déni de
service (lecture hors limites) à l'aide d'un fichier d'index de dépôt
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8099">CVE-2018-8099</a>

<p>Le renvoi d'un code d'erreur incorrect dans la fonction
index.c:read_entry() mène à une double libération de zone de mémoire dans
les versions de libgit2 antérieures à v0.26.2. Cela permet à un attaquant
de provoquer un déni de service à l'aide d'un fichier d'index de dépôt
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10887">CVE-2018-10887</a>

<p>Une extension de signe inattendue dans la fonction git_delta_apply dans
le fichier delta-apply.c pourrait conduire à un dépassement d'entier qui à
son tour mène à une lecture hors limite, permettant une lecture avant
l'objet de base. Un attaquant peut utiliser ce défaut pour divulguer des
adresses de mémoire ou provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10888">CVE-2018-10888</a>

<p>L'absence de vérification dans la fonction git_delta_apply dans le
fichier delta-apply.c, pourrait conduire à une lecture hors limite lors de
la lecture d'un fichier binaire delta. Un attaquant peut utiliser cela pour
provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15501">CVE-2018-15501</a>

<p>Dans ng_pkt dans transports/smart_pkt.c de libgit2, un attaquant distant
peut envoyer un paquet <q>ng</q> contrefait de protocole smart qui manque
d'un octet « \0 » pour déclencher une lecture hors limites qui mène à un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12278">CVE-2020-12278</a>

<p>path.c gère incorrectement les noms de fichiers équivalents qui
existent du fait des flux de données alternatifs de NTFS. Cela pourrait
permettre l'exécution de code distant lors du clonage d'un dépôt. Ce
problème est semblable au
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12279">CVE-2020-12279</a>

<p>checkout.c gère incorrectement les noms de fichiers équivalents qui
existent du fait des noms courts de NTFS. Cela pourrait permettre
l'exécution de code distant lors du clonage d'un dépôt. Ce problème est
semblable au
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 0.25.1+really0.24.6-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgit2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libgit2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libgit2">\
https://security-tracker.debian.org/tracker/libgit2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2936.data"
# $Id: $
