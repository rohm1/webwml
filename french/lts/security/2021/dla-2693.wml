#use wml::debian::translation-check translation="1c2bc7e2ac7f74ec3bf9c8f0d325b82dd26de1d0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les analyseurs XML, utilisés par XMLBeans, ne réglaient pas les propriétés
nécessaires pour protéger l’utilisateur d’une entrée XML malveillante. Les
vulnérabilités incluent la possibilité d’attaques par expansion d’entités
XML qui pourraient conduire à un déni de service. Cette mise à jour implémente
les valeurs par défaut judicieuses dans les analyseurs XML pour une prévention
de ce genre d’attaque.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2.6.0+dfsg-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xmlbeans.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xmlbeans, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/xmlbeans">\
https://security-tracker.debian.org/tracker/xmlbeans</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2693.data"
# $Id: $
