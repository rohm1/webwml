#use wml::debian::translation-check translation="5f8b9bbc6eb1520a574c5fe5892b1b15b74572f0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les CVE suivants ont été signalés à l’encontre de src:qemu.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9503">CVE-2017-9503</a>

<p>QEMU (c'est-à-dire Quick Emulator), lorsqu'il est construit avec la prise en charge
du contrôleur hôte de bus MegaRAID SAS 8708EM2, permet aux utilisateurs
privilégiés locaux de l’OS invité de provoquer un déni de service
(déréférencement de pointeur NULL et plantage du processus QEMU) à l’aide de
vecteurs impliquant le traitement d’une commande megasas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12068">CVE-2019-12068</a>

<p>Dans QEMU 1:4.1-1 (1:2.8+dfsg-6+deb9u8), lors de l’exécution d’un script dans
lsi_execute_script(), l’émulateur de l’adaptateur SCSI LSI avance l’indice
« s->dsp » pour lire l'opcode suivant. Cela peut conduire à une boucle infinie
si le prochain opcode est vide. Déplacement de la sortie de boucle existante
après 10 000 itérations afin de couvrir aussi les instructions nulles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20382">CVE-2019-20382</a>

<p>QEMU 4.1.0 possède une fuite de mémoire dans zrle_compress_data dans
ui/vnc-enc-zrle.c lors d’une opération de déconnexion VNC à cause de la mauvaise
utilisation de libz, aboutissant à une situation où la mémoire allouée dans
deflateInit2 n’est pas libérée dans deflateEnd.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1983">CVE-2020-1983</a>

<p>Une vulnérabilité d’utilisation de mémoire après libération dans ip_reass()
dans ip_input.c de libslirp 4.2.0 et des publications antérieures permet à des
paquets contrefaits de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8608">CVE-2020-8608</a>

<p>Dans libslirp 4.1.0, comme utilisé dans QEMU 4.2.0, tcp_subr.c utilise mal
les valeurs de retour snprintf, conduisant à un dépassement de tampon dans du
code postérieur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10756">CVE-2020-10756</a>

<p>Une vulnérabilité de lecture hors limites a été découverte dans
l’implémentation de réseautique SLiRP dans l’émulateur QEMU. Ce défaut apparaît
dans la routine icmp6_send_echoreply() lors de la réponse à une requête echo
ICMP, aussi connue comme ping. Ce défaut permet à un invité malveillant de faire
fuiter le contenu de la mémoire hôte, aboutissant à une possible divulgation
d'informations. Ce défaut affecte les versions de libslirp avant 4.3.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13361">CVE-2020-13361</a>

<p>Dans QEMU 5.0.0 et les versions antérieures, es1370_transfer_audio dans
hw/audio/es1370.c ne valide pas correctement le compte de trames. Cela permet
à des utilisateurs de l’OS client de déclencher un accès hors limites lors d’une
opération es1370_write().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13362">CVE-2020-13362</a>

<p>Dans QEMU 5.0.0 et les versions antérieures, megasas_lookup_frame dans
hw/scsi/megasas.c possède une lecture hors limites à l'aide d'un champ
reply_queue_head contrefait d’un utilisateur de l'OS client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13659">CVE-2020-13659</a>

<p>address_space_map dans exec.c dans QEMU 4.2.0 peut déclencher un
déréférencement de pointeur NULL relatif à BounceBuffer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13754">CVE-2020-13754</a>

<p>hw/pci/msix.c dans QEMU 4.2.0 permet aux utilisateurs d’OS client de
déclencher un accès hors limites à l'aide d'une adresse contrefaite dans une
opération msi-x mmio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13765">CVE-2020-13765</a>

<p>rom_copy() dans hw/core/loader.c dans QEMU 4.1.0 ne validait pas la relation
entre deux adresses. Cela permet à des attaquants de déclencher une opération
de copie mémoire non autorisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15863">CVE-2020-15863</a>

<p>Dépassement basé sur la pile dans xgmac_enet_send() dans hw/net/xgmac.c.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:2.8+dfsg-6+deb9u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2288.data"
# $Id: $
