#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1854">CVE-2015-1854</a>

<p>Un défaut a été découvert lors de l’établissement d’autorisation d’opérations
de modrdn. Un attaquant authentifié, pouvant présenter un appel ldapmodrdn au
serveur d’annuaire, pourrait réaliser des modifications non autorisées d’entrées
dans le serveur d’annuaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15134">CVE-2017-15134</a>

<p>Un traitement incorrect de filtre de recherche dans slapi_filter_sprintf()
dans slapd/util.c, peut conduire à un plantage de serveur distant et un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1054">CVE-2018-1054</a>

<p>Lorsque l’accès en lecture pour &lt;attribute_name&gt; est activé, un défaut
dans la fonction SetUnicodeStringFromUTF_8 dans collate.c, peut conduire à des
opérations en mémoire hors limites. Cela pourrait aboutir à un plantage de
serveur, causé par des utilisateurs non autorisés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1089">CVE-2018-1089</a>

<p>N’importe quel utilisateur (anonyme ou authentifié) peut planter ns-slapd
avec une requête ldapsearch contrefaite avec une valeur de filtre très longue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10850">CVE-2018-10850</a>

<p>Dû à une situation de compétition, le serveur pourrait planter dans le mode
turbo (à cause du trafic élevé) ou quand un travailleur (worker) lit plusieurs
requêtes dans le tampon de lecture (more_data). Ainsi un attaquant anonyme
pourrait déclencher un déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.3.5-4+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets 389-ds-base.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1428.data"
# $Id: $
