#use wml::debian::template title="Informations de sécurité" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7d2665e8ed8326e1216c8f61d4180eddc24d1266" maintainer="Jean-Pierre Giraud"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Garder un système Debian sûr</a></li>
<li><a href="#DSAS">Annonces récentes</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian prend les questions de sécurité très au sérieux. Nous traitons tous les problèmes de sécurité qui sont portés à notre attention et nous nous assurons qu'ils sont corrigés dans un délai raisonnable.</p>
</aside>

<p>
L'expérience a montré que <q>la sécurité par le secret</q> ne fonctionne jamais.
Une diffusion publique des problèmes de sécurité apporte plus rapidement des
solutions meilleures. Dans cet esprit, cette page indique l'état de Debian sur
différents trous de sécurité connus, qui pourraient potentiellement affecter
le système d'exploitation Debian.
</p>

<p>
Le projet Debian gère de nombreuses annonces de sécurité en coordination avec
les autres distributeurs de logiciel libre, et par conséquent, ces annonces sont
publiées le même jour que la vulnérabilité associée, et nous avons aussi notre
propre équipe <a href="audit/">d'audit de sécurité</a> qui est chargée
d'inspecter l'archive à la recherche de bogues de sécurité non corrigés.
</p>

# "reasonable timeframe" might be too vague, but we don't have
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.

<p>
Debian participe aussi aux efforts de standardisation de sécurité&nbsp;:
</p>

<ul>
  <li>Les <a href="#DSAS">annonces de sécurité Debian</a> (<i>Debian Security
Advisories</i>) sont <a href="cve-compatibility">compatibles avec le CVE</a>
(référez-vous aux  <a href="crossreferences">renvois croisés</a>).</li>
  <li>Debian est représentée dans le comité du projet
<a href="https://oval.cisecurity.org/">Open Vulnerability Assessment Language</a>.</li>
</ul>

<h2><a id="keeping-secure">Garder un système Debian sûr</a></h2>

<p>
Pour obtenir les dernières informations de sécurité de Debian, abonnez-vous à
la liste de diffusion (en anglais)
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>.
</p>

<p>En plus de cela, il est pratique d'utiliser
<a href="https://packages.debian.org/stable/admin/apt">APT</a> pour récupérer
les mises à jour relatives à la sécurité. Pour garder votre système
d'exploitation Debian à jour avec les correctifs de sécurité, ajoutez la ligne
suivante dans votre fichier <code>/etc/apt/sources.list</code>&nbsp;:</p>

<pre>
deb&nbsp;http://security.debian.org/debian-security&nbsp;<current_release_security_name>&nbsp;main&nbsp;contrib&nbsp;non-free
</pre>

<p>
Après avoir enregistré les modifications, exécutez les deux commandes suivantes
pour télécharger et installer les mises à jour en attente&nbsp;:
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre> 

<p>
L'archive de sécurité est signée avec les <a href="https://ftp-master.debian.org/keys.html">clefs</a> normales de l'archive Debian.
</p>

<p>
Pour plus d'information au sujet de la sécurité dans Debian, lisez la FAQ et la
documentation&nbsp;:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">FAQ sur la sécurité</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Manuel pour sécuriser Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Annonces récentes</a></h2>


<p>Ces pages web contiennent une archive condensée des annonces de sécurité qui
sont postées sur la liste de diffusion
<a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a>.
</p>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Annonces de sécurité Debian (uniquement les titres)" href="dsa.fr.rdf">
<link rel="alternate" type="application/rss+xml"
 title="Annonces de sécurité Debian (résumés)" href="dsa-long.fr.rdf">
:#rss#}

<p>
Les dernières informations de sécurité sont aussi disponibles sous la forme
de <a href="dsa.fr.rdf">fichiers RDF</a>. Nous vous proposons également une
<a href="dsa-long.fr.rdf">version légèrement plus longue</a> des fichiers qui
contient le premier paragraphe de l'alerte en question. Ainsi vous pourrez
facilement savoir de quoi traite le bulletin d'alerte.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>Les annonces de sécurité plus anciennes sont également disponibles&nbsp;:
<:= get_past_sec_list(); :>

<p>Les distributions Debian ne sont pas vulnérables à tous les problèmes de
sécurité. Le <a href="https://security-tracker.debian.org/">Debian Security
Tracker</a> collecte toutes les informations à propos de l'état de
vulnérabilité des paquets Debian. Il permet d'effectuer des recherches par
référence CVE ou par paquet.</p>
