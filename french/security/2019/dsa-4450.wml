#use wml::debian::translation-check translation="1f0499097245903ed56eee01d867c3b58d1dfac0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité a été découverte dans l'implémentation du protocole
WPA fournie dans wpa_supplication (station) et hostapd (point d'accès).</p>

<p>L'implémentation de EAP-pwd dans hostapd (serveur EAP) et wpa_supplicant
(pair EAP) ne valide pas correctement l'état de réassemblage de fragments
lors de la réception d'un fragment imprévu. Cela pourrait conduire au
plantage du processus dû à un déréférencement de pointeur NULL.</p>

<p>Un attaquant dans la portée d'une station ou d'un point d'accès prenant
en charge de EAP-pwd pourrait provoquer un plantage du processus
correspondant (wpa_supplicant ou hostapd), assurant un déni de service.</p>

<p>Pour la distribution stable (Stretch), ce problème a été corrigé dans la
version 2:2.4-1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpa.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpa, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4450.data"
# $Id: $
