#use wml::debian::translation-check translation="549ed0fce26a03a0a08c5aca5a6c51e4199c3e20" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Clôture de DebConf20 en ligne</define-tag>

<define-tag release_date>2020-08-30</define-tag>
#use wml::debian::news

<p>
Samedi 29 août 2020, la conférence annuelle des développeurs et
contributeurs Debian s'est achevée.
</p>

<p>
DebConf20 s'est tenue en ligne pour la première fois du fait de la
pandémie du coronavirus (COVID-19).
</p>

<p>
Toutes les sessions ont été diffusées en direct à travers différents canaux
pour participer à la conférence : messagerie IRC, édition de texte
collaboratif en ligne et salons de visioconférence.
</p>

<p>
Avec plus de 850 participants de 80 pays différents et un total de plus de
100 présentations, sessions de discussion, ou sessions spécialisées (BoF)
et d'autres activités,
<a href="https://debconf20.debconf.org">DebConf20</a> a été un énorme succès.
</p>

<p>
Dès qu'il est devenu évident que DebConf20 allait être un événement en
ligne, l'équipe vidéo de DebConf a passé beaucoup de temps les mois suivants
pour adapter, améliorer et dans certains cas écrire du début à la fin les
technologies nécessaires pour rendre possible la tenue de DebConf en ligne.
À partir des leçons tirées de la MiniDebConf en ligne fin mai, certaines
adaptations ont été réalisées et finalement nous avons mis au point une
configuration intégrant Jitsi, OBS, Voctomix, SReview, nginx, Etherpad et
un frontal récemment écrit basé sur le web pour Voctomix, pour former
l'ensemble des différents éléments nécessaires.
</p>

<p>
Toutes les composantes de l'infrastructure vidéo sont des logiciels libres
et l'ensemble des réglages est configuré au moyen de leur dépôt public
<a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a>.
</p>

<p>
Le <a href="https://debconf20.debconf.org/schedule/">programme </a> de
DebConf20 comprenait également deux programmes particuliers en une autre
langue que l'anglais : la MiniConf en espagnol avec huit communications en
deux jours et la MiniConf en malayalam avec neuf communications en trois
jours. Il a aussi été possible de réaliser, de diffuser et d'enregistrer
des activités ponctuelles introduites par les participants pendant toute la
durée de la conférence. Plusieurs équipes se sont aussi réunies pour des
<a href="https://wiki.debian.org/Sprints/">rencontres</a> sur certains
domaines de développement de Debian.
</p>

<p>
Entre les présentations, le flux vidéo présentait en boucle les parrains
habituels et également d'autres clips comprenant des photos des DebConf
précédentes, des anecdotes amusantes sur Debian et de courtes dédicaces
vidéos envoyées par des participants pour communiquer avec leurs amis de
Debian.
</p>

<p>
Pour tous ceux qui n'ont pu participer à la DebConf, la plupart des
communications et des sessions sont déjà disponibles sur le
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">site web des réunions Debian</a>,
et les dernières le seront dans les prochains jours.
</p>

<p>
Le site web de <a href="https://debconf20.debconf.org/">DebConf20</a>
restera actif à fin d'archive et continuera à offrir des liens vers les
présentations et vidéos des communications et des événements.
</p>

<p>
L'an prochain, il est prévu que <a href="https://wiki.debian.org/DebConf/21">DebConf21</a>
se tienne à Haïfa en Israël, en août ou septembre.
</p>

<p>
DebConf s'est engagée à offrir un environnement sûr et accueillant pour
tous les participants. Durant la conférence, plusieurs équipes (le
secrétariat, l'équipe d'accueil et l'équipe communauté) étaient disponibles
pour offrir aux participants le meilleur accueil à la conférence, et
trouver des solutions à tout problème qui aurait pu subvenir. Voir la
<a href="https://debconf20.debconf.org/about/coc/">page web à propos
du Code de conduite sur le site de DebConf20</a>
pour plus de détails à ce sujet.
</p>

<p>
Debian remercie les nombreux <a href="https://debconf20.debconf.org/sponsors/">parrains</a>
pour leur engagement dans leur soutien à DebConf20, et en particulier ses
parrains de platine :
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
et
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>.
</p>

<h2>À propos de Debian</h2>
<p>
Le projet Debian a été fondé en 1993 par Ian Murdock pour être un projet
communautaire réellement libre. Depuis cette date, le projet Debian est
devenu l'un des plus importants et des plus influents projets à code source
ouvert. Des milliers de volontaires du monde entier travaillent ensemble
pour créer et maintenir les logiciels Debian. Traduite en soixante-dix
langues et prenant en charge un grand nombre de types d'ordinateurs, la
distribution Debian est conçue pour être le <q>système d'exploitation
universel</q>.
</p>

<h2>À propos de DebConf</h2>

<p>
DebConf est la conférence des développeurs du projet Debian. En plus
d'un programme complet de présentations techniques, sociales ou
organisationnelles, DebConf fournit aux développeurs, aux contributeurs et
à toute personne intéressée, une occasion de rencontre et de travail
collaboratif interactif. DebConf a eu lieu depuis 2000 en des endroits du
monde aussi variés que l'Écosse, l'Argentine ou la Bosnie-Herzégovine. Plus
d'information sur DebConf est disponible à l'adresse
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>À propos de Lenovo</h2>

<p>
En tant que leader mondial en technologie, produisant une vaste gamme de
produits connectés, comprenant des smartphones, des tablettes, des
machines de bureau et des stations de travail aussi bien que des
périphériques de réalité augmentée et de réalité virtuelle, des solutions
pour la domotique ou le bureau connecté et les centres de données,
<a href="https://www.lenovo.com">Lenovo</a> a compris combien étaient
essentiels les plateformes et systèmes ouverts pour un monde connecté.
</p>

<h2>À propos d'Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> est la plus grande
compagnie suisse d'hébergement web qui offre aussi des services de
sauvegarde et de stockage, des solutions pour les organisateurs d'événement
et des services de diffusion en direct et de vidéo à la demande. Elle
possède l'intégralité de ses centres de données et de tous les éléments
essentiels pour le fonctionnement des services et produits qu'elle fournit
(à la fois sur le plan matériel et logiciel).
</p>

<h2>À propos de Google</h2>

<p>
<a href="https://google.com/">Google</a> est l'une des plus grandes
entreprises technologiques du monde qui fournit une large gamme de services
relatifs à Internet et de produits tels que des technologies de publicité
en ligne, des outils de recherche, de l'informatique dans les nuages, des
logiciels et du matériel.
</p>

<p>
Google apporte son soutien à Debian en parrainant la DebConf depuis plus de
dix ans, et est également un partenaire de Debian en parrainant les
composants de l'infrastructure d'intégration continue de
<a href="https://salsa.debian.org">Salsa</a> au sein de la plateforme
Google Cloud.
</p>

<h2>À propos d'Amazon Web Services (AWS)</h2>

<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> est une des
plateformes de nuage les plus complètes et largement adoptées au monde,
proposant plus de 175 services complets issus de centres de données du
monde entier (dans 77 zones de disponibilité dans 24 régions géographiques).
Parmi les clients d'AWS sont présentes des jeunes pousses à croissance
rapide, de grandes entreprises et des organisations majeures du secteur
public.
</p>

<h2>Plus d'informations</h2>

<p>Pour plus d'informations, veuillez consulter la page internet de
DebConf20 à l'adresse
<a href="https://debconf20.debconf.org/">https://debconf20.debconf.org/</a>
ou envoyez un message à &lt;press@debian.org&gt;.</p>
