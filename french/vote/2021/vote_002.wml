#use wml::debian::translation-check translation="6dec886d83814c13e7f8433ea16d80da02120a21" maintainer="Jean-Paul Guillonneau"
<define-tag pagetitle>Résolution générale : Déclaration à propos de la réadmission de Richard Stallman au conseil d’administration de la FSF</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Proposition et amendement ;</th>
        <td>24 mars 2021</td>
		<td></td>
      </tr>
      <tr>
        <th>Période de débat :</th>
		<td>24 mars 2021</td>
		<td></td>
      </tr>
       <tr>
         <th>Période de vote:</th>
            <td>Dimanche 4 avril 2021 00:00:00 UTC</td>
            <td>Samedi 17 avril 2021 23:59:59 UTC</td>
      </tr>
    </table>

    Le Responsable du projet a fixé la période minimale de débat à [<a href='https://lists.debian.org/debian-vote/2021/03/msg00115.html'>une semaine</a>]

    <vproposer />
    <p>Steve Langasek [<email vorlon@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00083.html'>texte de la proposition</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00095.html'>amendement</a>]
    </p>
    <vseconds />
    <ol>
       <li>Louis-Philippe Véronneau [<email pollo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00084.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00097.html'>approbation</a>] </li>
       <li>Joerg Jaspert [<email joerg@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00085.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00100.html'>approbation</a>] </li>
       <li>Neil McGovern [<email neilm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00086.html'>message</a>] </li>
       <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00087.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00096.html'>approbation</a>] </li>
       <li>Sam Hartman [<email hartmans@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00088.html'>message</a>] </li>
       <li>Nicolas Dandrimont [<email olasd@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00091.html'>message</a>] </li>
       <li>Colin Tuckley [<email colint@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00092.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00098.html'>approbation</a>]</li>
       <li>Paul R. Tagliamonte [<email paultag@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00161.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00103.html'>approbation</a>]</li>
    </ol>
    <vtext />
	<h3>Choix 1 : Appel à la démission du bureau de la FSF, comme dans rms-open-letter.github.io</h3>

<p>Le Projet Debian cosigne la déclaration concernant la réadmission de Richard
Stallman au conseil d’administration de la FSF pouvant être consultée sur
<a href='https://github.com/rms-open-letter/rms-open-letter.github.io/blob/main/index.md'>https://github.com/rms-open-letter/rms-open-letter.github.io/blob/main/index.md</a>.
Le texte de la déclaration est fourni ci-après.</p>

<p>Richard M. Stallman, couramment appelé RMS, constitue depuis longtemps un
danger pour la communauté du logiciel libre. Il s’est lui-même affiché comme
misogyne, capacitiste et transphobe, entre autres accusations graves. Ces
sortes d’idées n’ont pas leur place dans le logiciel libre, les droits
numériques et les communautés technologiques. Par le fait de sa réintégration
dans le conseil d’administration de la Free Software Foundation, nous appelons
le conseil en son entier à revenir sur cette décision et d’exclure RMS de tout
rôle de dirigeant.</p>

<p>Nous, les cosignataires, croyons à la nécessité de l’autonomie numérique et
à l’importance de la liberté de l’utilisateur pour la protection de nos droits
fondamentaux. Afin de réaliser la promesse que la liberté logicielle pour toutes
choses soit possible, un changement radical doit advenir dans la communauté.
Nous croyons à un présent et un futur où toutes les technologies augmentent les
droits des personnes, non pas leur oppression. Nous savons que cela n’est possible
que dans un monde où la technologie est construite pour respecter nos droits
dans tous leur aspects les plus fondamentaux. Quoique ces idées aient été
popularisées d’une certaine manière par Richard M. Stallman, il ne parle pas en
notre nom. Nous n’acceptons pas ses actions et ses opinions. Nous n’admettons
pas son commandement ou la direction de la Free Software tels qu’ils se
présentent aujourd’hui.

<p>Il y a eu trop de tolérance à propos du comportement et des idées répugnantes
de RMS. Nous ne pouvons permettre qu’une personne gâche la signification de
notre travail. Nos communautés n’ont pas de place pour des gens comme Richard
M. Stallman, et nous ne continuerons pas à souffrir de son comportement, en lui
donnant un rôle primordial, ou autrement en considérant son idéologie blessante
et dangereuse comme acceptable.

<p>Nous appelons à la démission du conseil d’administration de la Free Software
Foundation dans sa totalité. Ce sont des gens qui ont permis et soutenu RMS
depuis des années. Ils l’ont encore démontré en lui permettant de rejoindre le
bureau de la FSF. Il est temps que RMS se retire du logiciel libre, de l’éthique
technologique, des droits numériques parce qu’il ne peut nous fournir le
leadership dont nous avons besoin. Nous appelons aussi à ce que Richard
M. Stallman soit exclu de toutes ses positions de direction, y compris du
Projet GNU.

<p>Nous exhortons tous ceux en position de le faire d’arrêter de soutenir la
Free Software Foundation, à refuser de contribuer aux projets relatifs à la FSF
et RMS. Ne participez pas aux évènements de la FSF où à ceux qui accueillent RMS
et son image d’intolérance. Nous demandons aux contributeurs de projets de
logiciel libre de prendre position sur le sectarisme et la haine dans leurs
projets. En faisant cela, expliquez pourquoi à ces communautés et à la FSF.

<p><a href='https://rms-open-letter.github.io/appendix'>Nous avons rapporté
plusieurs incidents publics du comportement de RMS</a>.
Quelques-uns d’entre nous ont leur propre histoire sur RMS et leur relation, des
faits qui ne sont pas rapportés dans des courriels ou des vidéos. Nous espérons
que vous lirez ce qui a été partagé et considérerez le préjudice qu’il a causé
à notre communauté comme à d’autres.</p>

    <vproposerb />
    <p>Sruthi Chandran [<email srud@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00246.html'>texte de la proposition</a>]
    </p>
    <vsecondsb />
    <ol>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00253.html'>message</a>] </li>
	<li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00261.html'>message</a>] </li>
	<li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00265.html'>message</a>] </li>
	<li>Richard Laager [<email rlaager@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00266.html'>message</a>] </li>
	<li>Mike Gabriel [<email sunweaver@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00267.html'>message</a>] </li>
	<li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00269.html'>message</a>] </li>
	<li>Philip Hands [<email philh@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00270.html'>message</a>] </li>
	<li>Gard Spreemann [<email gspr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00272.html'>message</a>] </li>
	<li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00283.html'>message</a>] </li>
	<li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00292.html'>message</a>] </li>
    </ol>
    <vtextb />
	<h3>Choix 2 : Appel à la démission de Stallman de tous les organismes de la FSF</h3>
<p>Conformément à la section 4.1.5 de la Constitution, les développeurs ont fait la
déclaration suivante :</p>

<p>Déclaration de Debian à propos de Richard Stallman intégrant le bureau
directeur de la FSF</p>

<p>Nous, membres de Debian, sommes profondément déçus d’entendre que Richard
Stallman est réélu à une position dirigeante de la Free Software Foundation
après une série d’accusations sérieuses de mauvaise conduite ayant conduit
à sa démission de président et membre du bureau de la FSF en 2019.</p>

<p>Un facteur crucial dans le fait de rendre notre communauté plus ouverte est
de reconnaitre et mettre en avant quand d’autres personnes sont blessées par
nos propres actions et intégrer cela dans nos actions futures. La manière dont
Richard Stallman a annoncé son retour dans le bureau manque malheureusement
de toute considération de cette sorte de processus de réflexion. Nous sommes
profondément déçus que le conseil d’administration de la FSF l’ait de nouveau
élu comme membre du bureau en dépit qu’il n’ait fait aucune démarche
perceptible pour répondre, bien moins que faire amende honorable, de ses
anciennes actions ou des blessures infligées à d’autres. Enfin, nous sommes
aussi troublés par le processus mystérieux de sa réélection et par la façon
dont elle a été tardivement communiquée [0] aux collaborateurs et amis de la
FSF.</p>

<p>Nous pensons que cette démarche et la façon dont la communication
a été faite envoient un message mauvais et blessant et nuisent au futur du
mouvement du logiciel libre. Le but de celui-ci est de permettre à tout le monde
de contrôler sa technologie et donc de créer une meilleure société pour tout
un chacun. Le logiciel libre est destiné à tout le monde quelque soit son âge,
ses capacités ou son handicap, son identité sexuelle, son origine ethnique, sa
nationalité, sa religion ou son orientation sexuelle. Cela nécessite un
environnement ouvert et diversifié accueillant de la même manière tous les
contributeurs. Debian réalise que nous-mêmes et le mouvement du logiciel
libre doivent encore travailler dur pour parvenir à ce que chacun se sente en
sécurité et respecté pour participer, et parvenir à remplir la mission du
mouvement.</p>

<p>C’est pourquoi nous appelons à sa démission de tous les organismes de la
FSF. Celle-ci doit réfléchir sérieusement à sa décision ainsi qu’à la manière
dont elle a été prise pour prévenir des problèmes similaires dans le futur.
Par conséquent, dans la situation actuelle, nous sommes incapables de
collaborer à la fois avec la FSF et n’importe quelle autre organisation dans
laquelle Richard Stallman a une position dirigeante. À la place, nous
continuerons avec les individus et les groupes qui encouragent la diversité et
l’égalité dans le mouvement du logiciel libre pour remplir notre but commun
de permettre à tous les utilisateurs de contrôler leur technologie.</p>

[0] <a href='https://status.fsf.org/notice/3796703'>https://status.fsf.org/notice/3796703</a>

<br>
Basé en grande partie sur :<br>

[1] <a href='https://fsfe.org/news/2021/news-20210324-01.html'>https://fsfe.org/news/2021/news-20210324-01.html</a>
<br>
[2] <a href='https://www.eff.org/deeplinks/2021/03/statement-re-election-richard-stallman-fsf-board'>https://www.eff.org/deeplinks/2021/03/statement-re-election-richard-stallman-fsf-board</a>

    <vproposerc />
    <p>Santiago Ruano Rincón [<email santiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00344.html'>texte de la propositionl</a>]
[<a href='https://lists.debian.org/debian-vote/2021/03/msg00376.html'>amendement</a>]
    </p>
    <vsecondsc />
    <ol>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00345.html'>message</a>] </li>
	<li>Milan Kupcevic [<email milan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00347.html'>mail</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00379.html'>approbation</a>]</li>
	<li>Apollon Oikonomopoulos [<email apoikos@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00351.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00385.html'>approbation</a>] </li>
	<li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00353.html'>message</a>] </li>
	<li>Zlatan Todoric [<email zlatan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00363.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00377.html'>approbation</a>]</li>
    </ol>
    <vtextc />
	<h3>Choix 3 : Déconseiller de collaborer avec la FSF si Stallman fait partie des dirigeants</h3>

<p>Conformément à la section 4.1.5 de la Constitution, les développeurs ont fait la
déclaration suivante :</p>

<p>Déclaration de Debian à propos de Richard Stallman intégrant le bureau
directeur de la FSF</p>

<p>Nous, membres de Debian, sommes profondément déçus d’entendre que Richard
Stallman est réélu à une position dirigeante de la Free Software Foundation
après une série d’accusations sérieuses de mauvaise conduite ayant conduit
à sa démission de président et membre du bureau de la FSF en 2019.</p>

<p>Un facteur crucial dans le fait de rendre notre communauté plus ouverte est
de reconnaitre et mettre en avant quand d’autres personnes sont blessées par
nos propres actions et d’intégrer cela dans nos actions futures. La manière dont
Richard Stallman a annoncé son retour dans le bureau manque malheureusement
de toute considération de cette sorte de processus de réflexion. Nous sommes
profondément déçus que le conseil d’administration de la FSF l’ait de nouveau
élu comme membre du bureau en dépit qu’il n’ait fait aucune démarche
perceptible pour répondre, bien moins que faire amende honorable, de ses
anciennes actions ou des blessures infligées à d’autres. Enfin, nous sommes
aussi troublés par le processus mystérieux de sa réélection et par la façon
dont elle a été tardivement communiquée [0] aux collaborateurs et amis de la
FSF.</p>

<p>Nous pensons que cette démarche et la façon dont la communication
a été faite envoient un message mauvais et blessant et nuisent au futur du
mouvement du logiciel libre. Le but de celui-ci est de permettre à tout le monde
de contrôler sa technologie et donc de créer une meilleure société pour tout
un chacun. Le logiciel libre est destiné à tout le monde quelque soit son âge,
ses capacités ou son handicap, son identité sexuelle, son origine ethnique, sa
nationalité, sa religion ou son orientation sexuelle. Cela nécessite un
environnement ouvert et diversifié accueillant de la même manière tous les
contributeurs. Debian réalise que nous-mêmes et le mouvement du logiciel
libre doivent encore travailler dur pour parvenir à ce que chacun se sente en
sécurité et respecté pour participer, et parvenir à remplir la mission du
mouvement.</p>

<p>Par conséquent, dans la situation actuelle, le Projet Debian déconseille de
collaborer à la fois avec la FSF et n’importe quelle autre organisation dans
laquelle Richard Stallman a une position dirigeante. À la place, nous
continuerons avec les individus et les groupes qui encouragent la diversité et
l’égalité dans le mouvement du logiciel libre pour remplir notre but commun
de permettre à tous les utilisateurs de contrôler leur technologie.</p>

<p>[0] <a href='https://status.fsf.org/notice/3796703'>https://status.fsf.org/notice/3796703</a></p>

    <vproposerd />
    <p>Jonathan Wiltshire [<email jmw@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00391.html'>texte de la proposition</a>]
    </p>
    <vsecondsd />
    <ol>
	<li>Nicolas Dandrimont [<email olasd@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00393.html'>message</a>] </li>
	<li>Thadeu Lima de Souza Cascardo [<email cascardo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00395.html'>message</a>] </li>
	<li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00396.html'>message</a>] </li>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00401.html'>message</a>] </li>
	<li>Zlatan Todoric [<email zlatan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00402.html'>message</a>] </li>
	<li>Paride Legovini [<email paride@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00404.html'>message</a>] </li>
	<li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00405.html'>message</a>] </li>
	<li>Micha Lenk [<email micha@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00406.html'>message</a>] </li>
	<li>Gard Spreemann [<email gspr@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00408.html'>message</a>] </li>
	<li>Milan Kupcevic [<email milan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00410.html'>mail</a>] </li>
	<li>Richard Laager [<email rlaager@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00418.html'>message</a>] </li>
	<li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00422.html'>message</a>] </li>
	<li>Philipp Kern [<email pkern@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00066.html'>message</a>] </li>
    </ol>
    <vtextd />
	<h3>Choix 4 : Appel à la FSF de revoir ses processus de gouvernance</h3>

<p>Voici une prise de position des développeurs Debian conformément à notre
constitution, section 4.1.5.</p>

<p>Les développeurs croient fermement que les personnes influentes dans
n’importe quelle organisation prépondérante sont, et devraient être, tenues aux
normes les plus hautes de responsabilité.</p>

<p>Nous sommes vraiment désolés que les problèmes de transparence et de
responsabilité dans la gouvernance de la Free Software Foundation aient conduit
à des griefs sérieux et non résolus d’inconvenances de la part de son fondateur,
Richard Stallman, depuis un certain nombre d’années comme président ou membre
du bureau. En particulier, nous sommes profondément navrés que le bureau ait
jugé utile de le réintégrer sans bien prendre en compte les effets de ses
actions sur les plaignants.</p>

<p>Les développeurs acceptent que des personnes commettent des fautes, mais
croient que lorsque ces personnes sont dans une position influente, elles
doivent être responsables de leurs erreurs. Nous croyons que la chose la plus
importante lorsque des erreurs sont faites est d’apprendre de celles-ci et
de modifier son comportement. Nous sommes très inquiets par le fait que
Richard et le bureau n’aient pas suffisamment tenu compte des problèmes qui
ont touché un grand nombre de personnes et que Richard conserve une influence
significative sur le bureau de la FSF comme sur le projet GNU.</p>

<p>Nous exhortons la Free Software Foundation à revoir les mesures qu’elle
a prises en mars 2021 pour remanier la gouvernance de l’organisation et
à travailler inlassablement pour que ses engagements soient réalisés. Nous
croyons que c’est au travers d’une gouvernance pleinement responsable que les
membres d’une organisation sont certains que leur voix sera entendue. La Free
Software Foundation doit faire tout ce qui est en son pouvoir pour protéger
son personnel et ses membres, ainsi que la communauté dans son sens large,
y compris par un processus robuste et transparent pour traiter les plaintes.</p>

<p>Nous exhortons Richard Stallman et les autres membres du bureau qui l’ont
réintégré à revoir leurs positions.</p>

<p>Les développeurs sont fiers que les contributeurs au logiciel libre viennent
de toutes les branches de la société et que nos expériences et nos opinions
multiples sont une force pour le logiciel libre. Mais nous devons toujours
poursuivre nos efforts pour être certains que tous les contributeurs soient
traités avec égards et qu’ils se sentent en sécurité dans nos communautés – y
compris lors de rencontres physiques.</p>

    <vproposere />
    <p>Timo Weingärtner [<email tiwe@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00195.html'>texte de la proposition</a>]
    </p>
    <vsecondse />
    <ol>
	<li>Axel Beckert [<email abe@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00239.html'>message</a>] </li>
	<li>Lionel Élie Mamane [<email lmamane@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00338.html'>message</a>] </li>
	<li>Adam Borowski [<email kilobyte@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00359.html'>message</a>] </li>
	<li>Dmitry Smirnov [<email onlyjob@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00008.html'>message</a>] </li>
	<li>Erik Schanze [<email eriks@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00009.html'>message</a>] </li>
    </ol>
    <vtexte />
	<h3>Choix 5 : Soutenir la réintégration de Stallman, comme dans rms-support-letter.github.io</h3>
<p>Le Projet Debian cosigne la déclaration concernant la réadmission de Richard
Stallman au conseil d’administration de la FSF pouvant être consultée sur
https://rms-support-letter.github.io/. Le texte de la déclaration est fourni
ci-après.</p>

<p>Richard M. Stallman, couramment appelé RMS, a été une force motrice depuis
des décennies pour le mouvement du logiciel libre, avec des contributions
incluant notamment le système d’exploitation GNU et Emacs.</p>

<p>Récemment, de viles attaques ont été lancées en ligne cherchant à ce qu’il
soit démis du conseil d’administration de la FSF à cause de l’expression de
ses opinions personnelles. Nous avons déjà vu cela d’une manière organisée
avec d’autres activistes et programmeurs majeurs du logiciel libre. Nous ne
resterons pas les bras croisés cette fois-ci alors qu’une icône de la
communauté est attaquée.</p>

<p>La FSF est une entité autonome capable de traiter ses membres d’une façon
honnête et impartiale et ne devrait pas se soumettre à des pressions sociales
externes. Nous exhortons la FSF à considérer les arguments contre RMS
objectivement et à bien comprendre le sens de ses mots et de ses actions.</p>

<p>Historiquement, RMS a exprimé sa façon de voir d’une manière qui a
bouleversé beaucoup de gens. Il est généralement plus porté aux fondements
philosophiques et à la poursuite de la véracité objective et du purisme
linguistique, tout en sous-estimant les sentiments des personnes sur les
sujets qu’il commente. Cela rend ses arguments vulnérables à une mauvaise
compréhension et à une déformation des faits, chose que nous pensons qui arrive
dans la lettre appelant à sa démission. Ses mots doivent être interprétés
dans ce contexte et il doit être pris en compte que le plus souvent, il ne
cherche pas à présenter les choses de manière diplomatique.</p>

<p>Quoi qu’il en soit, l’opinion de Stallman sur le fait qu’il est persécuté
n’a aucun rapport avec sa capacité à diriger une société telle
que la FSF. De plus, il a le droit d’exprimer ses opinions comme tout un
chacun. Les membres et les sympathisants ne sont pas obligés d’être d’accord
avec ses opinions, mais doivent respecter son droit à la liberté de penser et
de s’exprimer.</p>

<h4>Concernant la FSF :</h4>

<p>La démission de RMS troublerai l’image de la FSF et serai un revers
important pour le dynamisme du mouvement du logiciel libre. Nous vous exhortons
à envisager vos actions soigneusement, puisque ce que vous déciderez aura un
impact sérieux sur le futur de l’industrie logicielle.</p>

<h4>Concernant le groupe en embuscade se liguant contre Richard Stallman et
contre ses arguments raisonnables dans le débat et ses opinions et convictions
diverses exprimées depuis des décennies en tant que personnalité publique :</h4>

<p>Vous n’êtes nullement concerné dans le choix de la gouvernance de n’importe
quelle communauté. Particulièrement, pas à travers une attaque groupée qui ne
ressemble en rien à un débat bien conduit comme l’ont illustré de meilleurs
gens tels que Richard Stallman.</p>

    <vproposerf />
    <p>Craig Sanders [<email cas@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00097.html'>texte de la proposition</a>]
    </p>
    <vsecondsf />
    <ol>
	<li>Adrian Bunk [<email bunk@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00187.html'>message</a>]</li>
	<li>Norbert Preining [<email preining@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00188.html'>message</a>]</li>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00189.html'>message</a>]</li>
	<li>Ying-Chun Liu [<email paulliu@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00190.html'>message</a>]</li>
	<li>Barak A. Pearlmutter [<email bap@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00191.html'>message</a>]</li>
	<li>Adam Borowski [<email kilobyte@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00196.html'>message</a>]</li>
	<li>Micha Lenk [<email micha@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00200.html'>message</a>]</li>
	<li>Michael Biebl [<email biebl@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/04/msg00209.html'>message</a>]</li>
    </ol>
    <vtextf />
	<h3>Choix 6 : Condamnation de la chasse aux sorcières à l’encontre de RMS et de la FSF</h3>

<p>Debian refuse de participer et condamne la chasse aux sorcières à l’encontre
de Richard, de la Free Software Foundation et des membres du bureau de la
Free Software Foundation.</p>

    <vproposera />
    <p>Timo Weingärtner [<email tiwe@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2021/03/msg00196.html'>texte de la proposition</a>]
[<a href='https://lists.debian.org/debian-vote/2021/03/msg00200.html'>amendement 1</a>]
[<a href='https://lists.debian.org/debian-vote/2021/03/msg00224.html'>amendement 2</a>]
[<a href='https://lists.debian.org/debian-vote/2021/03/msg00297.html'>amendement 3</a>]
    </p>
    <vsecondsa />
    <ol>
	<li>Bart Martens [<email bartm@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00201.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00322.html'>approbation</a>]</li>
	<li>Jonas Smedegaard [<email js@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00207.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00212.html'>approbation</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00232.html'>approbation</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00300.html'>approbation</a>] </li>
	<li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00215.html'>message</a>] </li>
        <li>Daniel Lenharo [<email lenharo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00219.html'>message</a>] </li>
	<li>Milan Kupcevic [<email milan@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00228.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00304.html'>approbation</a>]</li>
	<li>Michael Biebl [<email biebl@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00235.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00301.html'>approbation</a>]</li>
	<li>Axel Beckert [<email abe@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00236.html'>message</a>] </li>
	<li>Gilles Filippini [<email pini@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00241.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00327.html'>approbation</a>] </li>
	<li>Filippo Rusconi [<email lopippo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00252.html'>message</a>] </li>
	<li>Shengjing Zhu [<email zhsj@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00260.html'>message</a>] </li>
	<li>Matteo F. Vescovi [<email mfv@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00268.html'>message</a>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00314.html'>approbation</a>] </li>
	<li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/03/msg00299.html'>message</a>] </li>
    </ol>
    <vtexta />
	<h3>Choix 7 : Debian ne publiera pas de déclaration sur ce problème</h3>

<p>Le Projet Debian ne publiera pas de déclaration publique sur le fait que
Richard Stallman devrait être écarté ou non de ses positions dirigeantes.</p>

<p>Toute personne (y compris les membres de Debian) désirant (co)signer
n’importe quelle lettre ouverte sur ce sujet est invité à le faire
à titre personnel.</p>

   <vquorum />
    <p>
       Avec la liste actuelle des <a href="vote_002_quorum.log">développeurs
         ayant voté</a>, nous avons :
    </p>
   <pre>
#include 'vote_002_quorum.txt'
   </pre>
#include 'vote_002_quorum.src'



    <vstatistics />
    <p>
	Pour cette résolution générale, comme d'habitude,
#        <a href="https://vote.debian.org/~secretary/gr_rms/">des statistiques</a>
             des <a href="suppl_002_stats">statistiques</a>
             sur les bulletins et les accusés de réception sont rassemblées
             périodiquement durant la période du scrutin.
             De plus, la liste des votants
             sera enregistrée. La feuille d'émargement
             sera également disponible.
             De plus, la liste des <a href="vote_002_voters.txt">votants</a>
             sera enregistrée. La <a href="vote_002_tally.txt">feuille
             de compte</a> pourra être aussi consultée.
       </p>

    <vmajorityreq />
    <p>
      Les propositions ont besoin d’une majorité simple.
    </p>
#include 'vote_002_majority.src'

    <voutcome />
#include 'vote_002_results.src'

    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Secrétaire du projet Debian</a>
      </address>
