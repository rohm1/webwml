#use wml::debian::template title="Debian 10 -- Errata" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="68c554837d30857636a90637bd96af9d88c0b0a2"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Kända problem</toc-add-entry>
<toc-add-entry name="security">Säkerhetsproblem</toc-add-entry>

<p>Debians säkerhetsgrupp ger ut uppdateringar till paket i den stabila utgåvan
där de har identifierat problem relaterade till säkerhet. Vänligen se
<a href="$(HOME)/security/">säkerhetssidorna</a> för information om
potentiella säkerhetsproblem som har identifierats i <q>Buster</q>.</p>

<p>Om du använder APT, lägg till följande rad i <tt>/etc/apt/sources.list</tt>
för att få åtkomst till de senaste säkerhetsuppdateringarna:</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Punktutgåvor</toc-add-entry>

<p>Ibland, när det gäller kritiska problem eller säkerhetsuppdateringar,
uppdateras utgåvedistributionen. Generellt indikeras detta som punktutgåvor.
</p>

<ul>
  <li>Den första punktutgåvan 10.1, släpptes
      <a href="$(HOME)/News/2019/20190907">den 7 september 2019</a>.</li>
  <li>Den andra punktutgåvan 10.2, släpptes
      <a href="$(HOME)/News/2019/20191116">den 16 november 2019</a>.</li>
  <li>Den tredje punktutgåvan 10.3, släpptes
      <a href="$(HOME)/News/2020/20200208">den 8 februari 2020</a>.</li>
  <li>Den fjärde punktutgåvan 10.4, släpptes
      <a href="$(HOME)/News/2020/20200509">den 9 maj 2020</a>.</li>
  <li>Den femta punktutgåvan 10.5, släpptes
      <a href="$(HOME)/News/2020/20200801">den 1 augusti, 2020</a>.</li>
  <li>Den sjätte punktutgåvan 10.6, släpptes
      <a href="$(HOME)/News/2020/20200926">den 26 september, 2020</a>.</li>
  <li>Den sjunde punktutgåvan 10.7, släpptes
      <a href="$(HOME)/News/2020/20201205">den 5 december, 2020</a>.</li>
  <li>Den åttonde punktutgåvan 10.8, släpptes
      <a href="$(HOME)/News/2021/20210206">den 6 februari, 2021</a>.</li>
  <li>Den nionde punktutgåvan 10.9, släpptes
      <a href="$(HOME)/News/2021/20210327">den 27 mars, 2021</a>.</li>
  <li>Den tionde punktutgåvan 10.10, släpptes
      <a href="$(HOME)/News/2021/20210619">den 19 juni, 2021</a>.</li>
  <li>Den elfte punktutgåvan 10.11 släpptes
      <a href="$(HOME)/News/2021/2021100902">den 9 oktober, 2021</a>.</li>
  <li>Den tolfte punktutgåvan 10.12 släpptes
       <a href="$(HOME)/News/2022/2022032602">den 26 mars, 2022</a>.</li>


</ul>

<ifeq <current_release_buster> 10.0 "

<p>Det finns inga punktutgåvor för Debian 10 än.</p>" "


<p>Se <a
href=http://http.us.debian.org/debian/dists/buster/ChangeLog>\
förändringsloggen</a>
för detaljer om förändringar mellan 10 och <current_release_buster/>.</p>"/>

<p>Rättningar till den utgivna stabila utgåvan går ofta genom en
utökad testningsperiod innan de accepteras i arkivet. Dock, så finns dessa
rättningar tillgängliga i mappen
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> i alla Debianarkivsspeglingar.</p>

<p>Om du använder APT för att uppdatera dina paket kan du installera de
föreslagna uppdateringarna genom att lägga till följande rad i
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# föreslagna tillägg för en punktutgåva till Debian 10
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>Kör <kbd>apt update</kbd> efter detta, följt av
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installationssystem</toc-add-entry>

<p>
För mer information om kända problem och uppdateringar till
installationssystemet, se
<a href="debian-installer/">debian-installer</a>-sidan.
</p>
