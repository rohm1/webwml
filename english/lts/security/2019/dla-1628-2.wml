<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of jasper issued as DLA-1628-1 caused a regression due to
the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-19542">CVE-2018-19542</a>, a NULL pointer dereference in the function
jp2_decode, which could lead to a denial-of-service. In some cases not
only invalid jp2 files but also valid jp2 files were rejected.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.900.1-debian1-2.4+deb8u6.</p>

<p>We recommend that you upgrade your jasper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1628-2.data"
# $Id: $
