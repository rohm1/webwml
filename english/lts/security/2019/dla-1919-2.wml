<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<p>This updated advisory text mentions the additional non-security
changes and notes the need to install new binary packages.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0136">CVE-2019-0136</a>

    <p>It was discovered that the wifi soft-MAC implementation (mac80211)
    did not properly authenticate Tunneled Direct Link Setup (TDLS)
    messages.  A nearby attacker could use this for denial of service
    (loss of wifi connectivity).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9506">CVE-2019-9506</a>

    <p>Daniele Antonioli, Nils Ole Tippenhauer, and Kasper Rasmussen
    discovered a weakness in the Bluetooth pairing protocols, dubbed
    the <q>KNOB attack</q>.  An attacker that is nearby during pairing
    could use this to weaken the encryption used between the paired
    devices, and then to eavesdrop on and/or spoof communication
    between them.</p>

    <p>This update mitigates the attack by requiring a minimum encryption
    key length of 56 bits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11487">CVE-2019-11487</a>

    <p>Jann Horn discovered that the FUSE (Filesystem-in-Userspace)
    facility could be used to cause integer overflow in page reference
    counts, leading to a use-after-free.  On a system with sufficient
    physical memory, a local user permitted to create arbitrary FUSE
    mounts could use this for privilege escalation.</p>

    <p>By default, unprivileged users can only mount FUSE filesystems
    through fusermount, which limits the number of mounts created and
    should completely mitigate the issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15211">CVE-2019-15211</a>

    <p>The syzkaller tool found a bug in the radio-raremono driver that
    could lead to a use-after-free.  An attacker able to add and
    remove USB devices could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15212">CVE-2019-15212</a>

    <p>The syzkaller tool found that the rio500 driver does not work
    correctly if more than one device is bound to it.  An attacker
    able to add USB devices could use this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15215">CVE-2019-15215</a>

    <p>The syzkaller tool found a bug in the cpia2_usb driver that leads
    to a use-after-free.  An attacker able to add and remove USB
    devices could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15216">CVE-2019-15216</a>

    <p>The syzkaller tool found a bug in the yurex driver that leads to
    a use-after-free.  An attacker able to add and remove USB
    devices could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15218">CVE-2019-15218</a>

    <p>The syzkaller tool found that the smsusb driver did not validate
    that USB devices have the expected endpoints, potentially leading
    to a null pointer dereference.  An attacker able to add USB
    devices could use this to cause a denial of service (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15219">CVE-2019-15219</a>

    <p>The syzkaller tool found that a device initialisation error in the
    sisusbvga driver could lead to a null pointer dereference.  An
    attacker able to add USB devices could use this to cause a denial
    of service (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15220">CVE-2019-15220</a>

    <p>The syzkaller tool found a race condition in the p54usb driver
    which could lead to a use-after-free.  An attacker able to add and
    remove USB devices could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15221">CVE-2019-15221</a>

    <p>The syzkaller tool found that the line6 driver did not validate
    USB devices' maximum packet sizes, which could lead to a heap
    buffer overrun.  An attacker able to add USB devices could use
    this to cause a denial of service (memory corruption or crash) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15292">CVE-2019-15292</a>

    <p>The Hulk Robot tool found missing error checks in the Appletalk
    protocol implementation, which could lead to a use-after-free.
    The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15538">CVE-2019-15538</a>

    <p>Benjamin Moody reported that operations on XFS hung after a
    chgrp command failed due to a disk quota.  A local user on a
    system using XFS and disk quotas could use this for denial of
    service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15666">CVE-2019-15666</a>

    <p>The Hulk Robot tool found an incorrect range check in the network
    transformation (xfrm) layer, leading to out-of-bounds memory
    accesses.  A local user with CAP_NET_ADMIN capability (in any user
    namespace) could use this to cause a denial of service (memory
    corruption or crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15807">CVE-2019-15807</a>

    <p>Jian Luo reported that the Serial Attached SCSI library (libsas)
    did not correctly handle failure to discover devices beyond a SAS
    expander.  This could lead to a resource leak and crash (BUG).
    The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15924">CVE-2019-15924</a>

    <p>The Hulk Robot tool found a missing error check in the fm10k
    Ethernet driver, which could lead to a null pointer dereference
    and crash (BUG/oops).  The security impact of this is unclear.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15926">CVE-2019-15926</a>

    <p>It was found that the ath6kl wifi driver did not consistently
    validate traffic class numbers in received control packets,
    leading to out-of-bounds memory accesses.  A nearby attacker on
    the same wifi network could use this to cause a denial of service
    (memory corruption or crash) or possibly for privilege escalation.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.9.189-3~deb8u1.  This version also includes a fix for Debian bug
#930904, and other fixes included in upstream stable updates.</p>

<p>We recommend that you upgrade your linux-4.9 and linux-latest-4.9
packages.  You will need to use "apt-get upgrade --with-new-pkgs"
or <q>apt upgrade</q> as the binary package names have changed.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1919-2.data"
# $Id: $
