<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in jbig2dec, a JBIG2 decoder library.
One issue is related to an overflow with a crafted image file. The other
is related to a NULL pointer dereference.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
0.13-4.1+deb9u1.</p>

<p>We recommend that you upgrade your jbig2dec packages.</p>

<p>For the detailed security status of jbig2dec please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jbig2dec">https://security-tracker.debian.org/tracker/jbig2dec</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2796.data"
# $Id: $
