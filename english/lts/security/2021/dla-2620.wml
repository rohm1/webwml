<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a cross-site scripting (XSS) vulnerability
in python-bleach, a whitelist-based HTML sanitisation library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23980">CVE-2021-23980</a>

    <p>mutation XSS via allowed math or svg; p or br; and style, title,
    noscript, script, textarea, noframes, iframe, or xmp tags with
    strip_comments=False</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2.0-1+deb9u1.</p>

<p>We recommend that you upgrade your python-bleach packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2620.data"
# $Id: $
