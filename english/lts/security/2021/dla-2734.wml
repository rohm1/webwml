<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in curl,
a client-side URL transfer library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22898">CVE-2021-22898</a>

    <p>Information disclosure in connection to telnet servers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22924">CVE-2021-22924</a>

    <p>Bad connection reuse due to flawed path name checks.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
7.52.1-5+deb9u15.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">https://security-tracker.debian.org/tracker/curl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2734.data"
# $Id: $
