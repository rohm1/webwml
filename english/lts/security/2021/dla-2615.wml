<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Damian Lukowski discovered a flaw in spamassassin, a Perl-based spam
filter using text analysis. Malicious rule configuration files,
possibly downloaded from an updates server, could execute arbitrary
commands under multiple scenarios.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.4.2-1~deb9u4.</p>

<p>We recommend that you upgrade your spamassassin packages.</p>

<p>For the detailed security status of spamassassin please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/spamassassin">https://security-tracker.debian.org/tracker/spamassassin</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2615.data"
# $Id: $
