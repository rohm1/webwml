<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been found in a tool for processing triangulated solid
meshes admesh.</p>

<p>A heap-based buffer over-read in stl_update_connects_remove_1 (called from
stl_remove_degenerate) in connect.c was detected which might lead to memory
corruption and other potential consequences.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.98.2-3+deb9u1.</p>

<p>We recommend that you upgrade your admesh packages.</p>

<p>For the detailed security status of admesh please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/admesh">https://security-tracker.debian.org/tracker/admesh</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3019.data"
# $Id: $
