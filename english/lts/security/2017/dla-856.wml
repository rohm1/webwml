<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2017a. Notable
changes are:</p>

<ul>
<li>Mongolia no longer observes DST.</li>
<li>Magallanes region diverges from Santiago starting 2017-05-13,
   the America/Punta_Arenas zone has been added.</li>
</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2017a-0+deb7u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-856.data"
# $Id: $
