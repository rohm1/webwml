<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in PHP (recursive acronym for PHP:
Hypertext Preprocessor), a widely-used open source general-purpose
scripting language that is especially suited for web development and can
be embedded into HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7478">CVE-2016-7478</a>

    <p>Zend/zend_exceptions.c in PHP allows remote attackers to
    cause a denial of service (infinite loop) via a crafted Exception
    object in serialized data, a related issue to <a href="https://security-tracker.debian.org/tracker/CVE-2015-8876">CVE-2015-8876</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7479">CVE-2016-7479</a>

    <p>During the unserialization process, resizing the <q>properties</q> hash
    table of a serialized object may lead to use-after-free. A remote
    attacker may exploit this bug to gain the ability of arbitrary code
    execution. Even though the property table issue only affects PHP 7
    this change also prevents a wide range of other __wakeup() based
    attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7272">CVE-2017-7272</a>

    <p>The fsockopen() function will use the port number which is defined
    in hostname instead of the port number passed to the second
    parameter of the function. This misbehavior may introduce another
    attack vector for an already known application vulnerability (e.g.
    Server Side Request Forgery).</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.4.45-0+deb7u8.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-875.data"
# $Id: $
