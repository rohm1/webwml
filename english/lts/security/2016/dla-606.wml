<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in the tiff library, potentially
causing denial of services to applications using it.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u6.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-606.data"
# $Id: $
