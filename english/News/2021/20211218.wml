<define-tag pagetitle>Updated Debian 11: 11.2 released</define-tag>
<define-tag release_date>2021-12-18</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the second update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction authheaders "New upstream bug-fix release">
<correction base-files "Update /etc/debian_version for the 11.2 point release">
<correction bpftrace "Fix array indexing">
<correction brltty "Fix operation under X when using sysvinit">
<correction btrbk "Fix regression in the update for CVE-2021-38173">
<correction calibre "Fix syntax error">
<correction chrony "Fix binding a socket to a network device with a name longer than 3 characters when the system call filter is enabled">
<correction cmake "Add PostgreSQL 13 to known versions">
<correction containerd "New upstream stable release; handle ambiguous OCI manifest parsing [CVE-2021-41190]; support <q>clone3</q> in default seccomp profile">
<correction curl "Remove -ffile-prefix-map from curl-config, fixing co-installability of libcurl4-gnutls-dev under multiarch">
<correction datatables.js "Fix insufficient escaping of arrays passed to the HTML escape entities function [CVE-2021-23445]">
<correction debian-edu-config "pxe-addfirmware: Fix TFTP server path; improve support for LTSP chroot setup and maintenance">
<correction debian-edu-doc "Update Debian Edu Bullseye manual from the wiki; update translations">
<correction debian-installer "Rebuild against proposed-updates; update kernel ABI to -10">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction distro-info-data "Update included data for Ubuntu 14.04 and 16.04 ESM; add Ubuntu 22.04 LTS">
<correction docker.io "Fix possible change of host file system permissions [CVE-2021-41089]; lock down file permissions in /var/lib/docker [CVE-2021-41091]; prevent credentials being sent to the default registry [CVE-2021-41092]; add support for <q>clone3</q> syscall in default seccomp policy">
<correction edk2 "Address Boot Guard TOCTOU vulnerability [CVE-2019-11098]">
<correction freeipmi "Install pkgconfig files to correct location">
<correction gdal "Fix BAG 2.0 Extract support in LVBAG driver">
<correction gerbv "Fix out-of-bounds write issue [CVE-2021-40391]">
<correction gmp "Fix integer and buffer overflow issue [CVE-2021-43618]">
<correction golang-1.15 "New upstream stable release; fix <q>net/http: panic due to racy read of persistConn after handler panic</q> [CVE-2021-36221]; fix <q>archive/zip: overflow in preallocation check can cause OOM panic</q> [CVE-2021-39293]; fix buffer over-run issue [CVE-2021-38297], out of bounds read issue [CVE-2021-41771], denial of service issues [CVE-2021-44716 CVE-2021-44717]">
<correction grass "Fix parsing of GDAL formats where the description contains a colon">
<correction horizon "Re-enable translations">
<correction htmldoc "Fix buffer overflow issues [CVE-2021-40985 CVE-2021-43579]">
<correction im-config "Prefer Fcitx5 over Fcitx4">
<correction isync "Fix multiple buffer overflow issues [CVE-2021-3657]">
<correction jqueryui "Fix untrusted code execution issues [CVE-2021-41182 CVE-2021-41183 CVE-2021-41184]">
<correction jwm "Fix crash when using <q>Move</q> menu item">
<correction keepalived "Fix overly broad DBus policy [CVE-2021-44225]">
<correction keystone "Resolve information leak allowing determination of whether users exist [CVE-2021-38155]; apply some performance improvements to the default keystone-uwsgi.ini">
<correction kodi "Fix buffer overflow in PLS playlists [CVE-2021-42917]">
<correction libayatana-indicator "Scale icons when loading from file; prevent regular crashes in indicator applets">
<correction libdatetime-timezone-perl "Update included data">
<correction libencode-perl "Fix a memory leak in Encode.xs">
<correction libseccomp "Add support for syscalls up to Linux 5.15">
<correction linux "New upstream release; increase ABI to 10; RT: update to 5.10.83-rt58">
<correction linux-signed-amd64 "New upstream release; increase ABI to 10; RT: update to 5.10.83-rt58">
<correction linux-signed-arm64 "New upstream release; increase ABI to 10; RT: update to 5.10.83-rt58">
<correction linux-signed-i386 "New upstream release; increase ABI to 10; RT: update to 5.10.83-rt58">
<correction lldpd "Fix heap overflow issue [CVE-2021-43612]; do not set VLAN tag if client did not set it">
<correction mrtg "Correct errors in variable names">
<correction node-getobject "Resolve prototype pollution issue [CVE-2020-28282]">
<correction node-json-schema "Resolve prototype pollution issue [CVE-2021-3918]">
<correction open3d "Ensure that python3-open3d depends on python3-numpy">
<correction opendmarc "Fix opendmarc-import; increase maximum supported length of tokens in ARC_Seal headers, resolving crashes">
<correction plib "Fix integer overflow issue [CVE-2021-38714]">
<correction plocate "Fix an issue where non-ASCII characters would be wrongly escaped">
<correction poco "Fix installation of CMake files">
<correction privoxy "Fix memory leaks [CVE-2021-44540 CVE-2021-44541 CVE-2021-44542]; fix cross-site scripting issue [CVE-2021-44543]">
<correction publicsuffix "Update included data">
<correction python-django "New upstream security release: fix potential bypass of an upstream access control based on URL paths [CVE-2021-44420]">
<correction python-eventlet "Fix compatibility with dnspython 2">
<correction python-virtualenv "Fix crash when using --no-setuptools">
<correction ros-ros-comm "Fix denial of service issue [CVE-2021-37146]">
<correction ruby-httpclient "Use system certificate store">
<correction rustc-mozilla "New source package to support building of newer firefox-esr and thunderbird versions">
<correction supysonic "Symlink jquery instead of loading it directly; correctly symlink minimized bootstrap CSS files">
<correction tzdata "Update data for Fiji and Palestine">
<correction udisks2 "Mount options: Always use errors=remount-ro for ext filesystems [CVE-2021-3802]; use the mkfs command to format exfat partitions; add Recommends exfatprogs as preferred alternative">
<correction ulfius "Fix use of custom allocators with ulfius_url_decode and ulfius_url_encode">
<correction vim "Fix heap overflows [CVE-2021-3770 CVE-2021-3778], use after free issue [CVE-2021-3796]; remove vim-gtk alternatives during vim-gtk -&gt; vim-gtk3 transition, easing upgrades from buster">
<correction wget "Fix downloads over 2GB on 32-bit systems">
</table>


<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2021 4980 qemu>
<dsa 2021 4981 firefox-esr>
<dsa 2021 4982 apache2>
<dsa 2021 4983 neutron>
<dsa 2021 4984 flatpak>
<dsa 2021 4985 wordpress>
<dsa 2021 4986 tomcat9>
<dsa 2021 4987 squashfs-tools>
<dsa 2021 4988 libreoffice>
<dsa 2021 4989 strongswan>
<dsa 2021 4992 php7.4>
<dsa 2021 4994 bind9>
<dsa 2021 4995 webkit2gtk>
<dsa 2021 4996 wpewebkit>
<dsa 2021 4998 ffmpeg>
<dsa 2021 5002 containerd>
<dsa 2021 5003 ldb>
<dsa 2021 5003 samba>
<dsa 2021 5004 libxstream-java>
<dsa 2021 5007 postgresql-13>
<dsa 2021 5008 node-tar>
<dsa 2021 5009 tomcat9>
<dsa 2021 5010 libxml-security-java>
<dsa 2021 5011 salt>
<dsa 2021 5013 roundcube>
<dsa 2021 5016 nss>
<dsa 2021 5017 xen>
<dsa 2021 5019 wireshark>
<dsa 2021 5020 apache-log4j2>
<dsa 2021 5022 apache-log4j2>
</table>



<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>


