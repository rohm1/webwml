#use wml::debian::template title="Debian bullseye -- Guia de instalação" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="83895cd0b0913e07ec4171a51420513771cf1c64"

<if-stable-release release="stretch">
<p>Esta é uma <strong>versão beta</strong> do guia de instalação para o Debian
10, codinome buster, que ainda não foi lançado. A informação
apresentada aqui pode estar desatualizada e/ou incorreta devido a mudanças
no instalador. Você talvez se interesse pelo
<a href="../stretch/installmanual">guia de instalação para o Debian 9, codinome stretch</a>,
que é a última versão lançada do Debian; ou pela
<a href="https://d-i.debian.org/manual/">versão de desenvolvimento do guia de instalação</a>,
que é a versão mais atualizada deste documento.</p>
</if-stable-release>

<if-stable-release release="buster">
<p>Esta é uma <strong>versão beta</strong> do guia de instalação para o Debian
11, codinome bullseye, que ainda não foi lançado. A informação
apresentada aqui pode estar desatualizada e/ou incorreta devido a mudanças
no instalador. Você talvez se interesse pelo
<a href="../buster/installmanual">guia de instalação para o Debian 10, codinome buster</a>, 
que é a última versão lançada do Debian; ou pela
<a href="https://d-i.debian.org/manual/">versão de desenvolvimento do guia de instalação</a>, 
que é a versão mais atualizada deste documento.</p>
</if-stable-release>

<p>Instruções de instalação, junto aos arquivos que podem ser baixados, estão
disponíveis para cada arquitetura suportada:</p>

<ul>
<:= &permute_as_list('', 'Installation Guide'); :>
</ul>

<p>Se você definiu a localização do seu navegador de forma
apropriada, você pode usar o link acima para obter a versão HTML correta
automaticamente &mdash; veja sobre
<a href="$(HOME)/intro/cn">negociação de conteúdo</a>.
De outra forma, escolha a arquitetura exata, idioma e formato que você deseja
da tabela abaixo.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arquitetura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Idiomas</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   "$_[0].$_[1].$_[2]" } ); :>
</table>
</div>
