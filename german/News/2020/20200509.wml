<define-tag pagetitle>Debian 10 aktualisiert: 10.4 veröffentlicht</define-tag>
<define-tag release_date>2020-05-09</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="6547bb7720bfba1c2481b95c44642a4a6d3df030" maintainer="Erik Pfannenstein"

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die vierte Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>
<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction apt-cacher-ng "Abgesicherten Serveraufruf im Auslösemechanismus des Wartungsjobs erzwingen [CVE-2020-5202]; .zst-Kompression für Tarballs erlauben; Größe des Entpackungszeilenpuffers zum Lesen von Konfigurationsdateien vergrößert">
<correction backuppc "Den Benutzernamen beim »reload« an den Start-Stop-Daemon weitergeben, um Probleme zu vermeiden">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung">
<correction brltty "Dringlichkeit der Protokollmeldungen verringert, damit beim Einsatz zusammen mit neuen Orca-Versionen nicht zu viele Meldungen geschrieben werden">
<correction checkstyle "Problem mit XML External Entity Injection behoben [CVE-2019-9658 CVE-2019-10782]">
<correction choose-mirror "Enthaltene Liste der Spiegelserver aktualisiert">
<correction clamav "Neue Veröffentlichung der Originalautoren [CVE-2020-3123]">
<correction corosync "totemsrp: MTU reduzieren, damit keine übergroßen Pakete produziert werden">
<correction corosync-qdevice "Dienststart überarbeitet">
<correction csync2 "HELLO-Befehl fehlschlagen lassen, wenn SSL vorausgesetzt wird">
<correction cups "Heap-Pufferüberlauf [CVE-2020-3898] und <q>the `ippReadIO` function may under-read an extension field</q> behoben [CVE-2019-8842]">
<correction dav4tbsync "Neue Veröffentlichung der Originalautoren, welche die Kompatiblität mit neueren Thunderbird-Versionen wiederherstellt">
<correction debian-edu-config "Richtliniendatien für Firefox ESR und Thunderbird hinzugefügt, um Probleme beim Aufsetzen von TLS/SSL zu beheben">
<correction debian-installer "Aktualisierung auf das Kernel-ABI 4.19.0-9">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-security-support "Neue stabile Veröffentlichung der Originalautoren; Statusaktualisierung für mehrere Pakete; <q>runuser</q> anstelle von <q>su</q> verwenden">
<correction distro-info-data "Ubuntu 20.10 und voraussichtliches Enddatum für die Unterstützung von Stretch hinzugefügt">
<correction dojo "Ungeeignete Verwendung regulärer Ausdrücke überarbeitet [CVE-2019-10785]">
<correction dpdk "Neue stabile Veröffentlichung der Originalautoren">
<correction dtv-scan-tables "Neuer Schnappschuss der Originalautoren; alle derzeitigen deutschen DVB-T2-Muxes und den Eutelsat-5-West-A-Satelliten hinzugefügt">
<correction eas4tbsync "Neue Veröffentlichung der Originalautoren, um die Kompatibilität mit neueren Thunderbird-Versionen wiederherzustellen">
<correction edk2 "Sicherheitskorrekturen [CVE-2019-14558 CVE-2019-14559 CVE-2019-14563 CVE-2019-14575 CVE-2019-14586 CVE-2019-14587]">
<correction el-api "Upgrades von Stretch auf Buster überarbeitet, die mit Tomcat 8 zu tun haben">
<correction fex "Mögliches Sicherheitsproblem in fexsrv behoben">
<correction filezilla "Anfälligkeit auf Suchen in nicht vertrauenswürdigen Pfaden behoben [CVE-2019-5429]">
<correction frr "Extended-Next-Hop-Fähigkeit überarbeitet">
<correction fuse "Veraltete udevadm-Befehle aus den Nach-Installations-Skripten entfernt; beim vollständigen Deinstallieren nicht explizit die fuse.conf löschen">
<correction fuse3 "Veraltete udevadm-Befehle aus den Nach-Installations-Skripten entfernt; beim vollständigen Deinstallieren nicht explizit die fuse.conf löschen; Speicherleck in in fuse_session_new() behoben">
<correction golang-github-prometheus-common "Gültigkeit der Testzertifikate erweitert">
<correction gosa "(De)Serialisierung mit json_encode/json_decode ersetzt, um PHP-Objekt-Injektionen zu vermeiden [CVE-2019-14466]">
<correction hbci4java "EU-Direktive für Zahlungsdienste (PSD2) unterstützen">
<correction hibiscus "EU-Direktive für Zahlungsdienste (PSD2) unterstützen">
<correction iputils "Problem behoben, wegen welchem sich Ping unsauber mit Fehlercode beenden würde, wenn noch unversuchte Adressen im Rückgabewert des getaddrinfo()-Bibliotheksaufrufs auftauchen würden">
<correction ircd-hybrid "dhparam.pem benutzen, um einen Absturz beim Starten zu verhindern">
<correction jekyll "Verwendung von ruby-i18n 0.x und 1.x erlauben">
<correction jsp-api "Upgrades von Stretch auf Buster überarbeitet, die mit Tomcat 8 zu tun haben">
<correction lemonldap-ng "Unerwünschten Zugriff auf die Administrations-Endpunkte verhindern [CVE-2019-19791]; GrantSession-Plugin überarbeitet, welches keine Anmeldung verhindern konnte, wenn eine Zwei-Faktor-Authentifizierung verwendet wurde; eigenmächtige Weiterleitungen mit ODIC verhindert, wenn redirect_uri nicht verwendet wurde">
<correction libdatetime-timezone-perl "Enthaltene Daten aktualisiert">
<correction libreoffice "OpenGL-Folienübergänge überarbeitet">
<correction libssh "Mögliche Dienstblockade beim Umgang mit AES-CTR-Schlüsseln mittels OpenSSL behoben [CVE-2020-1730]">
<correction libvncserver "Heap-Überlauf behoben [CVE-2019-15690]">
<correction linux "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-latest "Kernel-ABI auf 4.19.0-9 aktualisiert">
<correction linux-signed-amd64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-arm64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-i386 "Neue stabile Veröffentlichung der Originalautoren">
<correction lwip "Pufferüberlauf verhindert [CVE-2020-8597]">
<correction lxc-templates "Neue stabile Veröffentlichung der Originalautoren; auch mit Sprachen umgehen, die nur UTF-8-encodiert sind">
<correction manila "Fehlende Zugriffsrechtekontrolle nachgerüstet [CVE-2020-9543]">
<correction megatools "Unterstützung für das neue Format der mega.nz-Links hinzugefügt">
<correction mew "Gültigkeitsprüfung des Server-SSL-Zertifikats überarbeitet">
<correction mew-beta "Gültigkeitsprüfung des Server-SSL-Zertifikats überarbeitet">
<correction mkvtoolnix "Neukompilierung, um die Abhängigkeit von libmatroska6v5 zu festigen">
<correction ncbi-blast+ "SSE4.2-Unterstützung abgeschaltet">
<correction node-anymatch "Unnötige Abhängigkeiten entfernt">
<correction node-dot "Codeausführung nach Prototype-Pollution verhindert [CVE-2020-8141]">
<correction node-dot-prop "Protoype-Polltuion behoben [CVE-2020-8116]">
<correction node-knockout "Maskierung mit älteren Internet-Explorer-Versionen überarbeitet [CVE-2019-14862]">
<correction node-mongodb "Ungültige _bsontypes zurückweisen [CVE-2019-2391 CVE-2020-7610]">
<correction node-yargs-parser "Prototype-Pollution behoben [CVE-2020-7608]">
<correction npm "Eigenmächtige Pfadzugriffe behoben [CVE-2019-16775 CVE-2019-16776 CVE-2019-16777]">
<correction nvidia-graphics-drivers "Neue stabile Veröffentlichung der Originalautoren">
<correction nvidia-graphics-drivers-legacy-390xx "Neue stabile Veröffentlichung der Originalautoren">
<correction nvidia-settings-legacy-340xx "Neue Veröffentlichung der Originalautoren">
<correction oar "Für die Perl-Funktion Storable::dclone auf Stretch-Verhalten zurückkehren, um Probleme mit der Rekursionstiefe zu lösen">
<correction opam "mcss aspcud vorziehen">
<correction openvswitch "Abbruch von vswitchd behoben, wenn ein Port hinzugefügt wird und der Controller nicht läuft">
<correction orocos-kdl "Zeichenkettenumwandlung mit Python 3 behoben">
<correction owfs "Defekte Python-3-Pakete entfernt">
<correction pango1.0 "Absturz in pango_fc_font_key_get_variations() behoben, wenn der Schlüssel leer ist">
<correction pgcli "Fehlende Abhängigkeit von python3-pkg-resources hinzugefügt">
<correction php-horde-data "Anfälligkeit für authentifizierte Code-Fernausführung behoben [CVE-2020-8518]">
<correction php-horde-form "Anfälligkeit für authentifizierte Code-Fernausführung behoben [CVE-2020-8866]">
<correction php-horde-trean "Anfälligkeit für authentifizierte Code-Fernausführung behoben [CVE-2020-8865]">
<correction postfix "Neue stabile Veröffentlichung der Originalautoren; Panik bei der Postfix multi-Milter-Konfiguration bei MAIL FROM behoben; Änderung bei d/init.d-Ausführung korrigiert, sodass es wieder mit mehreren Instanzen funktioniert">
<correction proftpd-dfsg "Speicherzugriffsproblem im keyboard-interative-Code in mod_sftp behoben; richtig mit DEBUG-, IGNORE-, DISCONNECT- und UNIMPLEMENTED-Nachrichten im keyboard-interactive-Modus umgehen">
<correction puma "Dienstblockadeproblem behoben [CVE-2019-16770]">
<correction purple-discord "Abstürze in ssl_nss_read behoben">
<correction python-oslo.utils "Durchsickern von empfindlichen Informationen via Mistral-Protokolle behoben [CVE-2019-3866]">
<correction rails "Mögliches seitenübergreifendes Skripting via JavaScript-Escape-Helper behoben [CVE-2020-5267]">
<correction rake "Anfälligkeit für Befehlsinjektion behoben [CVE-2020-8130]">
<correction raspi3-firmware "Diskrepanz in dtb-Namen in der z50-raspi-Firmware behoben; Startvorgang auf den Raspberry-Pi-Familien 1 und 0 überarbeitet">
<correction resource-agents "<q>ethmonitor does not list interfaces without assigned IP address</q> behoben; nicht länger benötigte xen-toolstack-Korrektur entfernt; nicht standardgemäße Verwendung im ZFS-Agenten behoben">
<correction rootskel "Unterstützung für mehrere Konsolen abschalten, wenn Preseeding verwendet wird">
<correction ruby-i18n "Gemspec-Generierung überarbeitet">
<correction rubygems-integration "Veraltet-Warnungen, wenn Anwender eine neuere Version der Rubygems via <q>gem update --system</q> installieren, vermeiden">
<correction schleuder "Korrektur überarbeitet, um mit den Codierungsfehlern umzugehen, die in der Vorversion entstanden sind; Standardcodierung auf UTF-8 geändert; x-add-key den Umgang mit Mails mit angehängten quoted-printable-codierten Schlüsseln ermöglichen; x-attach-listkey bei Mails, die von Thunderbird erstellt wurden und geschützte Kopfzeilen enthalten, korrigiert">
<correction scilab "Laden von Bibliotheken mit OpenJDK 11.0.7 überarbeitet">
<correction serverspec-runner "Ruby 2.5 unterstützen">
<correction softflowd "Fehler in Flow-Aggregation behoben, die zu Flow-Table-Überlauf und 100% CPU-Last führen können">
<correction speech-dispatcher "Pulsaudios Standard-Verzögerung korrigiert, die eine <q>kratzige</q> Tonausgabe verursachen kann">
<correction spl-linux "Deadlock behoben">
<correction sssd "sssd_be busy-looping bei unregelmäßiger LDAP-Verbindung behoben">
<correction systemd "beim Autorisieren via PolicyKit callback/userdata neu auflösen statt sie zwischenzuspeichern [CVE-2020-1712]; 60-block.rules in udev-udeb und initramfs-tools installiert">
<correction taglib "Korruptionsprobleme bei OGG-Dateien behoben">
<correction tbsync "Neue Veröffentlichung der Originalautoren, um die Kompatibilität mit Thunderbird wiederherzustellen">
<correction timeshift "Vorhersagbarkeit der Verwendung des Temporärverzeichnisses behoben [CVE-2020-10174]">
<correction tinyproxy "PIDDIR nur dann setzen, wenn PIDFILE eine Zeichenkette mit einer Länge größer null ist">
<correction tzdata "Neue stabile Veröffentlichung der Originalautoren">
<correction uim "Registrierung von Modulen, die nicht installiert sind, aufheben, um eine Regression im vorherigen Upload zu beseitigen">
<correction user-mode-linux "Kompilierungsfehlschlag mit den derzeitigen stabilen Kerneln behoben">
<correction vite "Absturz bei mehr als 32 Elementen behoben">
<correction waagent "Neue Veröffentlichung der Originalautoren; Ko-Installation mit cloud-init unterstützen">
<correction websocket-api "Upgrades von Stretch auf Buster überarbeitet, die mit Tomcat 8 zu tun haben">
<correction wpa "Beim PTK-Neuschlüsseln nicht versuchen, eine PSK-Diskrepanz zu ermitteln; auf FT-Unterstützung prüfen, wenn FT-Suiten gewählt werden; MAC-Randomisierungsproblem auf einigen Karten behoben">
<correction xdg-utils "xdg-open: pcmanfm-Prüfung und Umgang mit Verzeichnissen, die Leerzeichen im Namen haben, verbessert; xdg-screensaver: Fensternamen vor der Übermittlung via D-Bus überprüfen; xdg-mime: Konfigurationsverzeichnis anlegen, wenn es noch nicht existiert">
<correction xtrlock "Blockade (einiger) Multitouch-Geräte während Sperrung behoben [CVE-2016-10894]">
<correction zfs-linux "Potenzielle Deadlocks behoben">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>


<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4623 postgresql-11>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4627 webkit2gtk>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4636 python-bleach>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4638 chromium>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4641 webkit2gtk>
<dsa 2020 4642 thunderbird>
<dsa 2020 4643 python-bleach>
<dsa 2020 4644 tor>
<dsa 2020 4645 chromium>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4649 haproxy>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4651 mediawiki>
<dsa 2020 4652 gnutls28>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4654 chromium>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4658 webkit2gtk>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4661 openssl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4665 qemu>
<dsa 2020 4666 openldap>
<dsa 2020 4667 linux-signed-amd64>
<dsa 2020 4667 linux-signed-arm64>
<dsa 2020 4667 linux-signed-i386>
<dsa 2020 4667 linux>
<dsa 2020 4669 nodejs>
<dsa 2020 4671 vlc>
<dsa 2020 4672 trafficserver>
</table>

<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction getlive "Defekt wegen Hotmail-Änderungen">
<correction gplaycli "Defekt wegen Änderungen an der Google-API">
<correction kerneloops "Der dazugehörige Dienst ist nicht mehr verfügbar">
<correction lambda-align2 "[arm64 armel armhf i386 mips64el ppc64el s390x] Defekt auf Nicht-AMD64-Architekturen">
<correction libmicrodns "Sicherheitsprobleme">
<correction libperlspeak-perl "Sicherheitsprobleme; unbetreut">
<correction quotecolors "Inkompatibel mit neueren Thunderbird-Versionen">
<correction torbirdy "Inkompatibel mit neueren Thunderbird-Versionen">
<correction ugene "Unfrei; kompiliert nicht">
<correction yahoo2mbox "Seit mehreren Jahren defekt">

</table>


<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt; oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>
