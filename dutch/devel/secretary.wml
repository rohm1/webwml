#use wml::debian::template title="Secretaris van het Debian-project" BARETITLE="true" NOHEADER="true"
#use wml::debian::translation-check translation="965001d7d8f032b9d41bfa609f0cca3bc3c927d3"

    <h1 class="title">De Secretaris van het Debian-project</h1>

    <p class="initial">
      Naarmate het <a href="$(HOME)/">Debian-project</a> groeide, werd het
      duidelijk dat er een aantal semi-formele regels nodig was om te helpen bij
      het oplossen van conflicten en als gevolg hiervan werden de statuten
      opgesteld. De <a href="constitution">statuten</a> van Debian beschrijven
      de organisatiestructuur voor de formele besluitvorming in het project. De
      statuten bepalen wie de beslissingen neemt en welke bevoegdheden elk
      besluitvormend individu of orgaan heeft. Het Projectsecretariaat is een
      van de zes entiteiten die in de statuten vermeld worden als
      besluitvormingsentiteit.
    </p>

    <p>
      Elke Debian ontwikkelaar komt in aanmerking voor de functie van Debian
      Projectsecretaris. Iedereen mag meerdere functies bekleden, behalve dat
      de projectsecretaris niet ook de <a href="leader">projectleider van
      Debian</a> of de voorzitter van het <a href="tech-ctte">technisch
      comité</a> mag zijn.
    </p>

    <h2>Aanstelling</h2>

    <p>
      In tegenstelling tot andere afgevaardigden, die worden aangesteld door de
      <a href="leader">projectleider</a>, wordt de volgende projectsecretaris
      benoemd door de projectleider en de huidige projectsecretaris. Indien de
      huidige secretaris en de projectleider het niet eens zijn, moeten zij de
      ontwikkelaars via een algemene resolutie vragen een secretaris te
      benoemen.
    </p>

    <p>
      De ambtstermijn van de projectsecretaris bedraagt één jaar; daarna moet
      deze of een andere secretaris worden (her)benoemd.
    </p>

    <h2>Taken van de projectsecretaris</h2>


    <h3>Stemmingen houden</h3>
    <p>
      De meest zichtbare taak van de secretaris is het houden van
      <a href="$(HOME)/vote/">stemmingen</a> voor het project -- met name de
      verkiezing van de Projectleider (DPL), maar ook alle andere stemmingen die
      worden gehouden (algemene resoluties, bijvoorbeeld). Het houden van een
      stemming houdt ook in dat het aantal en de identiteit van de
      stemgerechtigden wordt bepaald met het oog op de berekening van het
      quorum.
    </p>

    <h3>Andere functionarissen vervangen</h3>

    <p>
      De projectsecretaris kan, samen met de voorzitter van het technisch
      comité, de leider vervangen. In deze situatie kunnen ze gezamenlijk
      besluiten nemen als ze deze hoognodig achten - maar alleen wanneer dit
      absoluut noodzakelijk is en alleen wanneer dit in overeenstemming is met
      de consensus onder de ontwikkelaars.
    </p>

    <p>
      Als er geen projectsecretaris is of de huidige secretaris niet
      beschikbaar is en de bevoegdheid voor het nemen van een besluit niet
      heeft gedelegeerd, kan het besluit worden genomen of gedelegeerd door de
      voorzitter van het technisch comité, als waarnemend secretaris.
    </p>

    <h3>De statuten interpreteren</h3>

    <p>
      De secretaris is ook verantwoordelijk voor de beslechting van geschillen
      over de interpretatie van de statuten.
    </p>


    <h2>Contactinformatie</h2>

    <p>U kunt contact opnemen met de projectsecretaris van Debian door een
    e-mail in het Engels te sturen naar <email "secretary@debian.org">.</p>


    <h1 class="center">Over onze huidige secretaris</h1>

    <p>
	  De huidige projectsecretaris is Kurt Roeckx
	  &lt;<email "kroeckx@debian.org">&gt;.
	  Kurt is sinds 1995 betrokken bij vrije software en Linux, en is een
      Debian-ontwikkelaar sinds
	  <a href='https://lists.debian.org/debian-project/2005/08/msg00283.html'>augustus 2005</a>.<br />
	  Kurt heeft een masterdiploma in elektronica en ICT, uitgereikt door
      de Hogeschool voor Wetenschap &amp; Kunst.
    </p>

	<p>
	  Kurt is projectsecretaris sinds februari 2009; hij nam de functie op zich
      nadat de vorige secretaris, Manoj Srivastava, ontslag had genomen. Naast
      projectsecretaris is Kurt betrokken bij het geschikt maken van Debian
      voor AMD64 en onderhoudt hij
      <a href="https://qa.debian.org/developer.php?login=kurt@roeckx.be">12
      pakketten</a>, waaronder libtool, ntp en openssl.
	</p>

	<h2>De functie van assistent-secretaris</h2>

	<p>
	  Door de toenemende hoeveelheid werk (d.w.z.: stemmingen) die de
      secretaris begon te moeten verwerken, besloten de DPL (destijds Steve
      McIntyre) en de vorige secretaris (Manoj) dat het een goed idee zou zijn
      om een assistent aan te stellen in geval van onbeschikbaarheid.<br />
	  Momenteel bekleedt Neil McGovern &lt;<email "neilm@debian.org">&gt;
      deze functie.
	</p>

    <hrline>
    <address>
      <a href="mailto:secretary@debian.org">De secretaris van het Debian-Project</a>
    </address>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:
