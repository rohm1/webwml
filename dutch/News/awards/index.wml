#use wml::debian::template title="Onderscheidingen"
#use wml::debian::translation-check translation="ccec77bd0e2877743e2f0c75235e272549af6ef8"

<p>Zowel onze distributie als deze website hebben verschillende
onderscheidingen gekregen van verschillende organisaties. Hieronder vindt u een
lijst met de onderscheidingen die we kennen. Als u er andere kent (of als u een
organisatie bent die de website of distributie een onderscheiding heeft
toegekend), stuur dan een e-mail naar
<a href="mailto:webmaster@debian.org">webmaster@debian.org</a> en we zullen het
hier vermelden.</p>

<h2>Onderscheidingen voor de distributie</h2>

<table width="100%">
 <colgroup span="2">
 <col align="left" width="50%">
 <col align="left" width="50%">
 </colgroup>
 <tr>
  <td>
    <p><b>Februari 2018</b></p>
    <p><q>Beste Linux-distributie</q>, 2018 Linux Journal Readers' Choice Awards (Onderscheiding toegekend door de lezers van Linux Journal)</p>
  </td>
  <td>
    <a href="https://www.linuxjournal.com/content/best-linux-distribution">
     <img src="ljaward2018.png" alt="Best Linux Distribution" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Februari 2012</b></p>
    <p><q>Serverdistributie van het jaar</q>, 2011 LinuxQuestions.org Members Choice Awards (Onderscheiding toegekend door de leden van LinuxQuestions.org)</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/linux-news-59/2011-linuxquestions-org-members-choice-award-winners-928502/">
     <img src="LinuxQuestions.png" alt="Server Distribution of the Year" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Augustus 2011</b></p>
    <p><q>Beste Linux-distributie van 2011</q> door <a href="http://www.tuxradar.com/content/best-distro-2011">TuxRadar</a></p>
  </td>
  <td>
    <a href="http://www.tuxradar.com/content/best-distro-2011">
     <img src="tuxradar-best-distro-2011.jpg" alt="Best Linux distro of 2011 by TuxRadar"></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Maart 2011</b></p>
    <p><q>Beste open source serverdistributie</q> en <q>Uitmuntende bijdrage aan open source/Linux/vrije software</q>
       op de <a href="$(HOME)/News/2011/20110304">Linux New Media Awards 2011</a> (Linux nieuwe media-onderscheiding)</p>
  </td>
  <td>
     <a href="http://www.linuxnewmedia.com/">
      <img src="lnm_award_2011.png" alt="Linux New media Award 2011"></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Februari 2011</b></p>
    <p><q>Serverdistributie van het jaar</q>, 2010 LinuxQuestions.org Members Choice Awards (Onderscheiding toegekend door de leden van LinuxQuestions.org)</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/linux-news-59/2010-linuxquestions-org-members-choice-award-winners-861440/">
     <img src="LinuxQuestions.png" alt="Server Distribution of the Year" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Februari 2010</b></p>
    <p><q>Serverdistributie van het jaar</q>, 2009 LinuxQuestions.org Members Choice Awards (Onderscheiding toegekend door de leden van LinuxQuestions.org)</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/2009-linuxquestions-org-members-choice-awards-91/server-distribution-of-the-year-780628/">
     <img src="LinuxQuestions.png" alt="Server Distribution of the Year" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Februari 2009</b></p>
    <p><q>Serverdistributie van het jaar</q>, 2008 LinuxQuestions.org Members Choice Awards (Onderscheiding toegekend door de leden van LinuxQuestions.org)</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/2008-linuxquestions.org-members-choice-awards-83/server-distribution-of-the-year-695611/">
     <img src="server-distribution-debian.png" alt="Server Distribution of the Year" /></a>
  </td>
 </tr>
 <tr>
  <td>
    <p><b>Februari 2008</b></p>
    <p><q>Serverdistributie van het jaar</q>, 2007 LinuxQuestions.org Members Choice Awards (Onderscheiding toegekend door de leden van LinuxQuestions.org)</p>
  </td>
  <td>
    <a href="http://www.linuxquestions.org/questions/2007-linuxquestions-org-members-choice-awards-79/server-distribution-of-the-year-610200/">
     <img src="LinuxQuestions.png" alt="Server Distribution of the Year" /></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>April 2006</b></p>
   <p><q>Aanbevolen project</q> door Linux+</p>
  </td>
  <td>
    <a href="http://lpmagazine.org/">
      <img src="rec_pro_L+.png" alt="Recommended Project by Linux+"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>November 2003</b></p>
   <p>Onderscheiding voor beste distributie voor Linux nieuwe media 2003.</p>
   <p>Vermelding in
      <a href="http://www.linux-magazin.de/Artikel/ausgabe/2003/12/award/award.html">\
      het decembernummer van 2003 (Duitstalig)</a> van Linux-Magazin.</p>
  </td>
  <td>
    <a href="http://www.linuxnewmedia.de/">
     <img src="lnm_award_logo_2003.jpg" alt="Linux New media Award"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Oktober 2003</b></p>
   <p>Beste distributie voor ondernemingen in Linux Enterprise Reader's Choice 2003. (Onderscheiding toegekend door de lezers van Linux Enterprise)
      (<a href="http://www.linuxenterprise.de/itr/service/show.php3?id=104&amp;nodeid=35">Results</a>)</p>
  </td>
  <td>
  </td>
 </tr>
 <tr>
  <td>
   <p>Beste Distributie in Linux Journal 2003 Readers' Choice Awards. (Onderscheiding toegekend door de lezers van Linux Journal)</p>
  </td>
  <td>
   <a href="http://pr.linuxjournal.com/article.php?sid=785">
    <img src="readerschoice2003small.png" width="128" height="122"
         alt="Linux Journal Readers' Choice 2003"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>Januari 2003</b></p>
   <p>Beste budgetkeuze voor Linux als server-besturingssysteem in Mikrodatorn 1/2003.</p>
  </td>
  <td>
   <a href="http://mikrodatorn.idg.se/">
    <img src="budgetval.png" alt="Bästa budgetval 2003" width="100" height="98"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>November 2002</b></p>
   <p>Eerste plaats in de categorie <q>Distributies</q> voor de onderscheiding
      Linux nieuwe media 2002.</p>
   <p>vermeld in het
      <a href="http://www.linux-magazin.de/Artikel/ausgabe/2002/12/award/award.html">\
      decembernummer van 2002 (Duits)</a> van Linux-Magazin. Een Engelse
      versie is beschikbaar als
      <a href="https://web.archive.org/web/20081203203144/http://www.linux-magazine.com/issue/25/2002_Linux_Awards.pdf">\
      PDF-bestand</a>.</p>
  </td>
  <td>
   <a href="http://www.linuxnewmedia.de/">
    <img src="lnm_award_logo.jpg" alt="Linux New media Award"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p><b>November 2000</b></p>
   <p>De VA Linux 2200 serie met Debian won de onderscheiding voor beste
      webserver in de Linux Journal 2000 Editors' Choice Awards. (Onderscheiding
      toegekend door de redacteurs van Linux Journal)</p>
  </td>
  <td>
   &nbsp;
  </td>
 </tr>
 <tr>
  <td>
   <p>Door lezers toegekende onderscheiding in de categorie infrastructuur op
      de conferentie en expositie over webhulpmiddellen.</p>
  </td>
  <td>
   &nbsp;
  </td>
 </tr>
 <tr>
  <td>
   <p><b>September 2000</b></p>
   <p>Tweede plaats in de categorie serverdistributie voor de onderscheiding
      toegekend door de redacteurs van Linux Magazine.</p>
  </td>
  <td>
   <a href="http://www.linux-mag.com/">
    <img src="EC_2000_lg.jpg" alt="Editor's Choice Award 2000"></a>
  </td>
 </tr>
 <tr>
  <td>
   <p>Winnaar van de <q>Favorieten van de LinuxWorld Conferentie &amp; Expo
   Show</q> in de categorie Distributie/Client.</p>
   <p>Finalist van de <q>Favorieten van de LinuxWorld Conferentie &amp; Expo
   Show</q> in de categorie Mail Server, Web Server, Distributie/Server.
  </td>
  <td>
   <img src="lwshowfav-200.gif"
    alt="LinuxWorld Conference &amp; Expo Show Favorites 1999">
  </td>
 </tr>
 <tr>
  <td>
   <p>De onderscheiding <q>sterk aangeprezen</q> toegekend door de redacteurs
      van APCMAG</p>
  </td>
  <td>
   &nbsp;
  </td>
 </tr>
</table>
