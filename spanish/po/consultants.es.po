# Traducción del sitio web de Debian
# Copyright (C) 2004 SPI Inc.
# Javier Fernández-Sanguino <jfs@debian.org>, 2004
#
msgid ""
msgstr ""
"Project-Id-Version: Templates webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2015-04-22 22:38+0100\n"
"Last-Translator: Laura Arjona Reina <larjona@larjona.net>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/consultants/consultant.defs:6
msgid "Name:"
msgstr "Nombre:"

#: ../../english/consultants/consultant.defs:9
msgid "Company:"
msgstr "Compañía:"

#: ../../english/consultants/consultant.defs:12
msgid "Address:"
msgstr "Dirección:"

#: ../../english/consultants/consultant.defs:15
msgid "Contact:"
msgstr "Contacto:"

#: ../../english/consultants/consultant.defs:19
msgid "Phone:"
msgstr "Teléfono:"

#: ../../english/consultants/consultant.defs:22
msgid "Fax:"
msgstr "Fax:"

#: ../../english/consultants/consultant.defs:25
msgid "URL:"
msgstr "URL:"

#: ../../english/consultants/consultant.defs:29
msgid "or"
msgstr "o"

#: ../../english/consultants/consultant.defs:34
msgid "Email:"
msgstr "Correo electrónico:"

#: ../../english/consultants/consultant.defs:52
msgid "Rates:"
msgstr "Tarifas:"

#: ../../english/consultants/consultant.defs:55
msgid "Additional Information"
msgstr "Información Adicional"

#: ../../english/consultants/consultant.defs:58
msgid "Willing to Relocate"
msgstr "Sin localización definida"

#: ../../english/consultants/consultant.defs:61
msgid ""
"<total_consultant> Debian consultants listed in <total_country> countries "
"worldwide."
msgstr ""
"<total_consultant> consultores de Debian en total en <total_country> países "
"por todo el mundo."

#: ../../english/template/debian/consultant.wml:6
msgid "List of Consultants"
msgstr "Lista de consultores"

#: ../../english/template/debian/consultant.wml:9
msgid "Back to the <a href=\"./\">Debian consultants page</a>."
msgstr "Volver a la <a href=\"./\">página de consultores de Debian</a>."
