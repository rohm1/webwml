#use wml::debian::translation-check translation="549ed0fce26a03a0a08c5aca5a6c51e4199c3e20"
<define-tag pagetitle>Clausura de DebConf20 online</define-tag>

<define-tag release_date>2020-08-30</define-tag>
#use wml::debian::news

<p>
El sábado 29 de agosto de 2020 se ha clausurado la conferencia anual de
desarrolladores y contribuidores de Debian.
</p>

<p>
DebConf20 ha sido la primera Debconf que se ha celebrado online, debido a la pandemia
de enfermedad por el coronavirus (COVID-19). 
</p>

<p>
Todas las sesiones se han retransmitido y han contado con diferentes formas de participación:
a través de mensajería IRC, de documentos de texto colaborativos
y de salas de reuniones virtuales.
</p>

<p>
Con más de 850 participantes de 80 países y un
total de más de 100 charlas, debates,
reuniones informales (BoF, por sus siglas en inglés: «Birds of a Feather») y otras actividades,
<a href="https://debconf20.debconf.org">DebConf20</a> ha sido un gran éxito.
</p>

<p>
Cuando quedó claro que DebConf20 iba a ser un evento exclusivamente
online, el equipo de vídeo de DebConf dedicó mucho tiempo de los meses siguientes a
adaptar, mejorar y, en algunos casos, escribir desde cero la tecnología que
sería necesaria para hacer posible una DebConf online. Con las lecciones
aprendidas en la MiniDebConfOnline celebrada a finales de mayo, se hicieron algunos
ajustes y, finalmente, llegamos a una configuración con Jitsi, OBS,
Voctomix, SReview, nginx, Etherpad y una interfaz web para voctomix
recién escrita, todo ello formando parte de la pila de elementos.
</p>

<p>
Todos los componentes de la infraestructura de vídeo son software libre y
todo el sistema se configura a través de su repositorio
<a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a> público.
</p>

<p>
La <a href="https://debconf20.debconf.org/schedule/">programación</a> de la DebConf20 incluía
dos canales de habla no inglesa: la MiniConf en español,
con ocho charlas a lo largo de dos días,
y la MiniConf en malabar, con nueve charlas en tres días.
También ha sido posible realizar, retransmitir y grabar actividades ad hoc planificadas por
asistentes a la conferencia durante el desarrollo de la misma. Asimismo, ha habido varias
reuniones de equipos para <a href="https://wiki.debian.org/Sprints/">esprintar</a> en determinadas áreas de desarrollo de Debian.
</p>

<p>
Entre charla y charla, la retransmisión de vídeo ha estado mostrando el habitual bucle de patrocinadores, pero también
algunos clips adicionales con fotos de DebConfs anteriores, hechos divertidos sobre Debian
y breves vídeos de reconocimiento enviados por los asistentes para comunicarse con sus amigos de Debian.
</p>

<p>
Para quienes no pudieron participar, la mayoría de las charlas y sesiones ya están
disponibles en el
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">archivo web de encuentros Debian</a>,
y el resto lo estará en los próximos días.
</p>

<p>
El sitio web <a href="https://debconf20.debconf.org/">DebConf20</a> 
permanecerá activo como archivo y continuará ofreciendo
enlaces a las presentaciones y vídeos de charlas y eventos.
</p>

<p>
Para el próximo año está prevista la celebración de <a href="https://wiki.debian.org/DebConf/21">DebConf21</a>
en Haifa, Israel, en agosto o septiembre.
</p>

<p>
La DebConf está comprometida con el establecimiento de un ambiente seguro y acogedor para todos y todas las participantes.
Durante la conferencia han estado disponibles varios equipos (Recepción, Equipo de bienvenida y Equipo
de comunidad) para ayudar a que quienes han participado hayan vivido la mejor experiencia
durante la conferencia y para ayudar a encontrar soluciones a cualquier problema que haya podido surgir.
Consulte la <a href="https://debconf20.debconf.org/about/coc/">página sobre el código de conducta en el sitio web de la DebConf20</a>
para más detalles sobre esta cuestión.
</p>

<p>
Debian agradece el compromiso de numerosos <a href="https://debconf20.debconf.org/sponsors/">patrocinadores</a>
por su apoyo a la DebConf20, especialmente el de nuestros patrocinadores platino:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
y 
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>.
</p>

<h2>Acerca de Debian</h2>
<p>
El proyecto Debian fue fundado en 1993 por Ian Murdock para ser
un proyecto comunitario verdaderamente libre. Desde entonces el proyecto
ha crecido hasta ser uno de los proyectos más grandes e importantes de software libre.
Miles de voluntarios de todo el mundo trabajan juntos para crear y mantener
programas para Debian. Se encuentra traducido a 70 idiomas y soporta
una gran cantidad de arquitecturas de ordenadores, por lo que el proyecto
se refiere a sí mismo como <q>el sistema operativo universal</q>.
</p>

<h2>Acerca de DebConf</h2>

<p>
DebConf es la conferencia de desarrolladores del proyecto Debian. Además de un
amplio programa de charlas técnicas, sociales y sobre reglamentación, DebConf proporciona una
oportunidad para que desarrolladores, contribuidores y otras personas interesadas se
conozcan en persona y colaboren más estrechamente. Se ha celebrado
anualmente desde 2000 en lugares tan diversos como Escocia, Argentina y
Bosnia-Herzegovina. Hay más información sobre DebConf disponible en
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>Acerca de Lenovo</h2>

<p>
Como líder tecnológico global en la fabricación de un amplio catálogo de productos conectados
incluyendo teléfonos inteligentes, tabletas, PC y estaciones de trabajo, dispositivos de realidad aumentada y de realidad virtual,
soluciones de hogar, oficina y centros de datos inteligentes, <a href="https://www.lenovo.com">Lenovo</a>
entiende lo críticos que son los sistemas y plataformas abiertos para un mundo conectado.
</p>

<h2>Acerca de Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> es la mayor empresa de alojamiento web de Suiza.
También ofrece servicios de respaldo y de almacenamiento, soluciones para organizadores de eventos y
servicios bajo demanda de retransmisión en directo («live-streaming») y vídeo.
Es propietaria de sus centros de proceso de datos y de todos los elementos críticos
para la prestación de los servicios y productos que suministra
(tanto software como hardware). 
</p>

<h2>Acerca de Google</h2>

<p>
<a href="https://google.com/">Google</a> es una de las mayores empresas tecnológicas del
mundo y proporciona un amplio rango de servicios y productos relacionados con Internet tales
como tecnologías de publicidad en línea, búsqueda, computación en la nube, software y hardware.
</p>

<p>
Google ha dado soporte a Debian patrocinando la DebConf más de
diez años y es también socio de Debian patrocinando partes
de la infraestructura de integración continua de <a href="https://salsa.debian.org">Salsa</a>
en la plataforma de la nube de Google («Google Cloud Platform»).
</p>

<h2>Acerca de Amazon Web Services (AWS)</h2>

<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> es una de las plataformas
de nube más completas y más ampliamente adoptadas del mundo,
que ofrece más de 175 servicios con funcionalidades completas desde centros de datos globalizados
(en 77 zonas de disponibilidad dentro de 24 regiones geográficas).
Entre los clientes de AWS se encuentran las startups de más rápido crecimiento, las empresas más grandes
y agencias gubernamentales destacadas.
</p>

<h2>Información de contacto</h2>

<p>Para más información, visite la página web de DebConf20 en
<a href="https://debconf20.debconf.org/">https://debconf20.debconf.org/</a>
o envíe un correo electrónico a &lt;press@debian.org&gt;.</p>
