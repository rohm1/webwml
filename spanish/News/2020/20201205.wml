#use wml::debian::translation-check translation="5c427e44dc4a1503d5262b5edfba60b490ea0ab1"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.7</define-tag>
<define-tag release_date>2020-12-05</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la séptima actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction base-files "Actualizado para esta versión">
<correction choose-mirror "Actualiza la lista de réplicas">
<correction cups "Corrige free() inválido de «printer-alert»">
<correction dav4tbsync "Nueva versión del proyecto original, compatible con versiones más recientes de Thunderbird">
<correction debian-installer "Usa la ABI del núcleo Linux 4.19.0-13; añade grub2 a Built-Using">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction distro-info-data "Añade Ubuntu 21.04, Hirsute Hippo">
<correction dpdk "Nueva versión «estable» del proyecto original; corrige problema de ejecución de código remoto [CVE-2020-14374], problemas TOCTOU [CVE-2020-14375], desbordamiento de memoria [CVE-2020-14376], lectura de memoria fuera de límites [CVE-2020-14377] y desbordamiento negativo («underflow») de entero [CVE-2020-14377]; corrige compilación en armhf con NEON">
<correction eas4tbsync "Nueva versión del proyecto original, compatible con versiones más recientes de Thunderbird">
<correction edk2 "Corrige desbordamiento de entero en DxeImageVerificationHandler [CVE-2019-14562]">
<correction efivar "Añade soporte para dispositivos nvme-fabrics y nvme-subsystem; corrige variable no inicializada en parse_acpi_root, evitando posible violación de acceso">
<correction enigmail "Introduce asistente de migración al soporte de GPG incluido en Thunderbird">
<correction espeak "Corrige el uso de espeak con mbrola-fr4 cuando mbrola-fr1 no está instalado">
<correction fastd "Corrige fuga de contenido de la memoria al recibir demasiados paquetes inválidos [CVE-2020-27638]">
<correction fish "Se asegura de restaurar las opciones de TTY al salir">
<correction freecol "Corrige vulnerabilidad de entidad externa XML [CVE-2018-1000825]">
<correction gajim-omemo "Usa vector de inicialización de doce bytes, para mayor compatibilidad con clientes iOS">
<correction glances "Por omisión, escucha solo en localhost">
<correction iptables-persistent "No fuerza la carga de módulos del núcleo; mejora la lógica de eliminación de reglas">
<correction lacme "Usa cadena de certificados del proyecto original en lugar de una predefinida en el código, facilitando el soporte para los nuevos certificados raíz e intermedio de Let's Encrypt">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos a tzdata 2020d">
<correction libimobiledevice "Añade soporte parcial de iOS 14">
<correction libjpeg-turbo "Corrige denegación de servicio [CVE-2018-1152], lectura de memoria fuera de límites [CVE-2018-14498], posible ejecución de código remoto [CVE-2019-2201] y lectura de memoria fuera de límites [CVE-2020-13790]">
<correction libxml2 "Corrige denegación de servicio [CVE-2017-18258], desreferencia de puntero NULL [CVE-2018-14404], bucle infinito [CVE-2018-14567], fuga de contenido de la memoria [CVE-2019-19956 CVE-2019-20388] y bucle infinito [CVE-2020-7595]">
<correction linux "Nueva versión «estable» del proyecto original">
<correction linux-latest "Actualizado para la ABI del núcleo 4.19.0-13">
<correction linux-signed-amd64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-arm64 "Nueva versión «estable» del proyecto original">
<correction linux-signed-i386 "Nueva versión «estable» del proyecto original">
<correction lmod "Cambia arquitectura a <q>any</q> - requerido debido a que LUA_PATH y LUA_CPATH se determinan en tiempo de compilación">
<correction mariadb-10.3 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2020-14765 CVE-2020-14776 CVE-2020-14789 CVE-2020-14812 CVE-2020-28912]">
<correction mutt "Se asegura de cerrar la conexión IMAP después de un error de conexión [CVE-2020-28896]">
<correction neomutt "Se asegura de cerrar la conexión IMAP después de un error de conexión [CVE-2020-28896]">
<correction node-object-path "Corrige contaminación de prototipo en set() [CVE-2020-15256]">
<correction node-pathval "Corrige contaminación de prototipo [CVE-2020-7751]">
<correction okular "Corrige ejecución de código por medio de enlaces de acción [CVE-2020-9359]">
<correction openjdk-11 "Nueva versión del proyecto original; corrige caída de la JVM">
<correction partman-auto "Aumenta el tamaño de /boot en la mayoría de las recetas a valores de entre 512 y 768M para gestionar mejor cambios en la ABI del núcleo y sistemas de archivos de RAM inicial («initramfs») mayores; limita el tamaño de la RAM como se hace para los cálculos de la partición de swap, resolviendo problemas en máquinas con más RAM que espacio en disco">
<correction pcaudiolib "Limita la latencia de cancelación a 10ms">
<correction plinth "Apache: inhabilita mod_status [CVE-2020-25073]">
<correction puma "Corrige problemas de inyección de HTTP y de «contrabando» de HTTP («HTTP smuggling») [CVE-2020-5247 CVE-2020-5249 CVE-2020-11076 CVE-2020-11077]">
<correction ros-ros-comm "Corrige desbordamiento de entero [CVE-2020-16124]">
<correction ruby2.5 "Corrige potencial vulnerabilidad de «contrabando» de peticiones HTTP («HTTP request smuggling») en WEBrick [CVE-2020-25613]">
<correction sleuthkit "Corrige desbordamiento de pila en yaffsfs_istat [CVE-2020-10232]">
<correction sqlite3 "Corrige división por cero [CVE-2019-16168], desreferencia de puntero NULL [CVE-2019-19923], tratamiento erróneo de nombre de ruta NULL durante una actualización de un archivo ZIP [CVE-2019-19925], tratamiento erróneo de NULs embebidos en nombres de fichero [CVE-2019-19959], posible caída (eliminación de elementos de la pila WITH) [CVE-2019-20218], desbordamiento de entero [CVE-2020-13434], violación de acceso [CVE-2020-13435], problema de «uso tras liberar» [CVE-2020-13630], desreferencia de puntero NULL [CVE-2020-13632] y desbordamiento de memoria dinámica («heap») [CVE-2020-15358]">
<correction systemd "Basic/cap-list: analiza sintácticamente e imprime capacidades numéricas; reconoce capacidades nuevas del núcleo Linux 5.8; networkd: no genera MAC para dispositivo puente de red («bridge»)">
<correction tbsync "Nueva versión del proyecto original, compatible con versiones más recientes de Thunderbird">
<correction tcpdump "Corrige problema de entrada no confiable en la impresión de PPP [CVE-2020-8037]">
<correction tigervnc "Almacena correctamente excepciones de certificados en visor VNC nativo y java [CVE-2020-26117]">
<correction tor "Nueva versión «estable» del proyecto original; varias correcciones de seguridad, facilidad de uso, portabilidad y fiabilidad">
<correction transmission "Corrige fuga de contenido de la memoria">
<correction tzdata "Nueva versión del proyecto original">
<correction ublock-origin "Nueva versión del proyecto original; distribuye extensión («plugin») en paquetes específicos del navegador">
<correction vips "Corrige uso de variable no inicializada [CVE-2020-20739]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2020 4766 rails>
<dsa 2020 4767 mediawiki>
<dsa 2020 4768 firefox-esr>
<dsa 2020 4769 xen>
<dsa 2020 4770 thunderbird>
<dsa 2020 4771 spice>
<dsa 2020 4772 httpcomponents-client>
<dsa 2020 4773 yaws>
<dsa 2020 4774 linux-latest>
<dsa 2020 4774 linux-signed-amd64>
<dsa 2020 4774 linux-signed-arm64>
<dsa 2020 4774 linux-signed-i386>
<dsa 2020 4774 linux>
<dsa 2020 4775 python-flask-cors>
<dsa 2020 4776 mariadb-10.3>
<dsa 2020 4777 freetype>
<dsa 2020 4778 firefox-esr>
<dsa 2020 4779 openjdk-11>
<dsa 2020 4780 thunderbird>
<dsa 2020 4781 blueman>
<dsa 2020 4782 openldap>
<dsa 2020 4783 sddm>
<dsa 2020 4784 wordpress>
<dsa 2020 4785 raptor2>
<dsa 2020 4786 libexif>
<dsa 2020 4787 moin>
<dsa 2020 4788 firefox-esr>
<dsa 2020 4789 codemirror-js>
<dsa 2020 4790 thunderbird>
<dsa 2020 4791 pacemaker>
<dsa 2020 4792 openldap>
<dsa 2020 4793 firefox-esr>
<dsa 2020 4794 mupdf>
<dsa 2020 4795 krb5>
<dsa 2020 4796 thunderbird>
<dsa 2020 4798 spip>
<dsa 2020 4799 x11vnc>
<dsa 2020 4800 libproxy>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction freshplayerplugin "No soportado por navegadores; proyecto original discontinuado">
<correction nostalgy "Incompatible con versiones más recientes de Thunderbird">
<correction sieve-extension "Incompatible con versiones más recientes de Thunderbird">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
