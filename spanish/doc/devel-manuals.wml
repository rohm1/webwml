#use wml::debian::ddp title="Manuales para desarrolladores de Debian"
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b" maintainer="Laura Arjona Reina"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"

<document "Manual de Normas de Debian (Debian Policy)" "policy">

<div class="centerblock">
<p>
  Este manual describe las normas requeridas por la distribución Debian
  GNU/Linux. Incluye la estructura y contenido del archivo de Debian,
  varias cuestiones relativas al diseño del sistema operativo, así como
  requerimientos técnicos que cada paquete debe contemplar para ser
  incluido en la distribución.

<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "The Debian Policy group">
  <status>
  Disponible
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p><a href="https://bugs.debian.org/debian-policy">Enmiendas propuestas</a> a la normativa

  <p>Documentos de normativa suplementaria:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Estándar de jerarquía de directorios («Filesystem Hierarchy Standard»)</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">texto sencillo</a>]
    <li><a href="debian-policy/upgrading-checklist.html">Lista de control de actualización</a>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">Lista de nombres de paquetes virtuales</a>
    <li><a href="packaging-manuals/menu-policy/">Normativa para menús</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">texto sencillo</a>]
    <li><a href="packaging-manuals/perl-policy/">Normativa de Perl</a>
    [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">texto sencillo</a>]
    <li><a href="packaging-manuals/debconf_specification.html">especificación debconf</a>
    <li><a href="packaging-manuals/debian-emacs-policy">Normativa Emacsen</a>
    <li><a href="packaging-manuals/java-policy/">Normativa Java</a>
    <li><a href="packaging-manuals/python-policy/">Normativa Python</a>
    <li><a href="packaging-manuals/copyright-format/1.0/">especificación del formato de copyright</a>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Referencia del Desarrollador de Debian" "devref">

<div class="centerblock">
<p>
  Este manual describe procedimientos y recursos para los mantenedores de
  Debian. Describe cómo llegar a ser desarrollador, el proceso de envío de
  paquetes (<q>upload</q>), cómo controlar el sistema de seguimiento de errores,
  las listas de correo, servidores en Internet, etc.

  <p>Este documento está pensado como <em>manual de referencia</em> para
  todos los desarrolladores de Debian (novatos y veteranos).

<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  Disponible
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Guía para Mantenedores de Debian"
"debmake-doc">

<div class="centerblock">
<p>
Este documento guía describe la construcción de los paquetes Debian a
usuarios comunes y futuros desarrolladores usando la orden
<code>debmake</code>.
</p>
<p>
Está enfocada en el estilo moderno de empaquetado y viene con muchos
ejemplos sencillos.
</p>
<ul>
<li>Empaquetado de shell script POSIX</li>
<li>Empaquetado de script en Python3</li>
<li>C con Makefile/Autotools/CMake</li>
<li>paquetes de múltiples binarios con biblioteca compartida etc.</li>
</ul>
<p>
Esta “Guía para Mantenedores de Debian” puede considerarse como la
sucesora de la “Guía del Nuevo Mantenedor de Debian”.
</p>

<doctable>
  <authors "Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  Disponible
  </status>
  <availability>
  <inpackage "debmake-doc">
  <inddpvcs-debmake-doc>
  </availability>
</doctable>
</div>

<hr>

<document "Guía del Nuevo Mantenedor de Debian" "maint-guide">

<div class="centerblock">
<p>
  Este documento intentará describir la manera de construir
  un paquete de Debian GNU/Linux para el usuario común de Debian
  (y futuro desarrollador) en un lenguaje sencillo y explicado
  con ejemplos prácticos.

  <p>Al contrario que los intentos anteriores, este documento se basa en
  <code>debhelper</code> y las nuevas herramientas disponibles.
  El autor está haciendo todo lo posible para incorporar y unificar
  los esfuerzos anteriores.

<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  Obsoleto, use la «Guía para Mantenedores de Debian» (debmake-doc)
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Introducción al empaquetado en Debian" "packaging-tutorial">

<div class="centerblock">
<p>
Este tutorial es una introducción al empaquetado en Debian.
Enseña a los futuros desarrolladores 
cómo modificar paquetes existentes, cómo crear sus propios paquetes,
y cómo interactuar con la comunidad de Debian.
Además del tutorial principal, incluye tres sesiones prácticas sobre modificar el paquete <code>grep</code>,
y empaquetar el juego <code>gnujump</code> y una biblioteca Java.
</p>

<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  Disponible
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr>

<document "El Sistema de Menús de Debian" "menu">

<div class="centerblock">
<p>
  Este manual describe el sistema de menús de Debian y el paquete
  <strong>menu</strong>.

  <p>El paquete menu se inspiró en el programa install-fvwm2-menu del
  viejo paquete fvwm2. Sin embargo, menu intenta proporcionar
  una interfaz más general para construir menús.
  Con la orden update-menus de este paquete, no hace falta
  modificar ningún otro paquete para cada nuevo gestor de
  ventanas de X, a la vez que proporciona una interfaz
  unificada tanto para programas gráficos como de texto.

<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  Disponible
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML en línea</a>
  </availability>
</doctable>
</div>

<hr>

<document "El Instalador de Debian por dentro" "d-i-internals">

<div class="centerblock">
<p>
  Este documento pretende hacer el Instalador de Debian más accesible a nuevos
  desarrolladores, como un sitio centralizado para documentar información técnica.

<doctable>
  <authors "Frans Pop">
  <maintainer "Debian Installer team">
  <status>
  Disponible
  </status>
  <availability>
  <p><a href="https://d-i.debian.org/doc/internals/">HTML en línea</a></p>
  <p><a href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">fuentes DocBook XML en línea</a></p>
  </availability>
</doctable>
</div>

<hr>

<document "documentación de dbconfig-common" "dbconfig-common">

<div class="centerblock">
<p>
  Este documento está dirigido a los encargados de paquetes que mantienen paquetes
  que requieren una base de datos en funcionamiento. En vez de implementar la lógica para
  preguntar por la configuración de la base de datos en las fases de <tt>install</tt>,
  <tt>upgrade</tt>, <tt>reconfigure</tt>, y <tt>deinstall</tt> pueden dejarle este
  trabajo a dbconfig-common que se encargará de crear y configurar la base de datos.

<doctable>
  <authors "Sean Finney y Paul Gevers">
  <maintainer "Paul Gevers">
  <status>
  Disponible
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbconfig-common>
  Adicionalmente también está disponible el <a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">documento de diseño</a>.
  </availability>
</doctable>
</div>

<hr>

<document "dbapp-policy" "dbapp-policy">

<div class="centerblock">
<p>
  Una normativa propuesta para paquetes que dependen en una base de datos funcionando.

<doctable>
  <authors "Sean Finney">
  <maintainer "Paul Gevers">
  <status>
  Borrador
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbapp-policy>
  </availability>
</doctable>
</div>

