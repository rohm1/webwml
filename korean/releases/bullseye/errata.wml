#use wml::debian::template title="데비안 11 -- 정오표" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="2c12526e7c785d665d8d8cae4b75533f682d1365" maintainer="Sebul"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Known problems</toc-add-entry>
<toc-add-entry name="security">보안 이슈</toc-add-entry>

<p>데비안 보안 팀은 보안 관련 문제를 식별한 안정 릴리스 패키지에 대한 업데이트를 발행합니다.
 <q>bullseye</q>에서 식별된 보안 이슈는
<a href="$(HOME)/security/">보안 페이지</a>를 보세요.</p>

<p>APT를 쓴다면, 최근 보안 업데이트에 접근하려면 아래 행을 <tt>/etc/apt/sources.list</tt>에 더하세요:</p>

<pre>
  deb http://security.debian.org/debian-security bullseye-security main contrib non-free
</pre>

<p>그 다음, <kbd>apt update</kbd> 실행하고
<kbd>apt upgrade</kbd> 하세요.</p>


<toc-add-entry name="pointrelease">포인트 릴리스</toc-add-entry>

<p>때때로, 여러 심각한 문제 또는 보안 업데이트가 있으면, 
릴리스 배포가 업데이트 됩니다. 일반적으로, 포인트 릴리스로 표시됩니다.</p>

<ul>
  <li>첫 포인트 릴리스, 11.1은
      <a href="$(HOME)/News/2021/20211009">2021.10.9.</a> 나왔습니다.</li>
  <li>두번째 포인트 릴리스, 11.2는
      <a href="$(HOME)/News/2021/20211218">2021.12.18.</a> 나왔습니다.</li>
  <li>세번째 포인트 릴리스, 11.3은
      <a href="$(HOME)/News/2022/20220326">2022. 3. 26.</a> 나왔습니다.</li>

</ul>

<ifeq <current_release_bullseye> 11.0 "

<p>There are no point releases for Debian 11 yet.</p>" "

<p>데비안 11과 <current_release_bullseye/> 차이를 자세히 보려면 <a
href="http://http.us.debian.org/debian/dists/bullseye/ChangeLog">\
ChangeLog</a> 를 보세요.</p>"/>


<p>Fixes to the released stable distribution often go through an
extended testing period before they are accepted into the archive.
However, these fixes are available in the
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> directory of any Debian archive
mirror.</p>

<p>apt를 써서 패키기를 업데이트하면, 제안된 업데이트를 다음 줄을 
<tt>/etc/apt/sources.list</tt>에 추가하여 설치할 수 있습니다:</p>

<pre>
  \# proposed additions for a 11 point release
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>그 다음, <kbd>apt update</kbd> 그 다음
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">설치 시스템</toc-add-entry>

<p>
설치 시스템의 정오표 및 업데이트 정보는<a href="debian-installer/">설치 정보</a> 페이지를 보세요.
</p>
